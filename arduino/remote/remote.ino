#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>
#include <RF24_config.h>

#include <Adafruit_Sensor.h>
//#include <Wire.h>
#include <Adafruit_BMP085_U.h>

#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN    3    //DHT PIN
#define DHTTYPE   DHT22     // DHT 22 (AM2302)

//RADIO RF24 PINS
#define RF_CE    7
#define RF_CSN   8


// Sleep time between sensor updates (in milliseconds)
// Must be >1000ms for DHT22 and >2000ms for DHT11
static const uint64_t UPDATE_INTERVAL = 3000;

//Temperature
DHT_Unified dht(DHTPIN, DHTTYPE);

//Pression
Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(10085);


//Radio
RF24 radio(RF_CE,RF_CSN);
const byte address[6] = "00001";

//Other

//Communication structure
struct Data {
  long type;
  float value;
} data;

//Polling variables & interval
long previousMillis = 0;
long interval = UPDATE_INTERVAL;  

/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/
void displaySensorDetails(sensor_t sensor, String type, String unit)
{
  Serial.println("------------------------------------");
  Serial.println(type);
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(unit);
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(unit);
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(unit);
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

void initRadio() {
  radio.begin();
  radio.setAutoAck(false);
  //radio.setRetries(15, 15);
  radio.setPALevel(RF24_PA_HIGH);
  radio.openWritingPipe(address);
}

void setup() {
    Serial.begin(9600);
    
    /* Initialise the sensors */
    dht.begin();
    if(!bmp.begin())
    {
        // There was a problem detecting the BMP085 ... check your connections
        Serial.print("Ooops, no BMP085 detected ... Check your wiring or I2C ADDR!");
        while(1);
    }

    Serial.println("DHTxx Unified Sensor Example");
    sensor_t sensor;

    // Print temperature sensor details.
    dht.temperature().getSensor(&sensor);
    displaySensorDetails(sensor, "Temperature", "%");

    // Print humidity sensor details.
    dht.humidity().getSensor(&sensor);
    displaySensorDetails(sensor, "Humidity", "%");

    /* Display some basic information on this sensor */
    bmp.getSensor(&sensor);
    displaySensorDetails(sensor, "Pressure", "hPa");

    initRadio();
}

void sendMessage(long type, float value){
  Serial.println("sendMessage");
  data.type = type;
  data.value = value;
  if(radio.write(&data, sizeof(data))){
    Serial.println("Send SUCCESS");
  }else {
    Serial.println("Send FAIL");
  }
  delay(100);
}

void loop() {
  // Delay between measurements.
  unsigned long currentMillis = millis();
  
  if(currentMillis - previousMillis > interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;  

    // Get temperature event and print its value.
    sensors_event_t event;  
    dht.temperature().getEvent(&event);
    float temperature = event.temperature;
    if (isnan(temperature)) {
      Serial.println("Error reading temperature!");
    }
    else {
      Serial.print("Temperature: ");
      Serial.print(temperature);
      Serial.println(" *C");
      sendMessage(0, temperature);
      //sendMessage(0, 20.10);
    }
    // Get humidity event and print its value.
    dht.humidity().getEvent(&event);
    float humidity = event.relative_humidity;
    if (isnan(humidity)) {
      Serial.println("Error reading humidity!");
    }
    else {
      Serial.print("Humidity: ");
      Serial.print(humidity);
      Serial.println("%");
      sendMessage(1, humidity);
      //sendMessage(1, 45.3);
    }
    // Get pression event and print its value
    bmp.getEvent(&event);
    float pressure = event.pressure;
    if (isnan(pressure)) {
      Serial.println("Error reading humidity!");
    }
    else {
      Serial.print("Pressure:    ");
      Serial.print(pressure);
      Serial.println(" hPa");
      sendMessage(2, pressure);
      //sendMessage(2, 1033);
    }
  }
  
}
