#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>
#include <RF24_config.h>
#include <SPI.h>


//RADIO RF24 PINS
#define RF_CE    7
#define RF_CSN   8

//Radio
RF24 radio(RF_CE,RF_CSN);
const byte address[6] = "00001";

//Communication structure
struct Data {
  long type;
  float value;
} data;


/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/

void initRadio() {
  radio.begin();
  radio.setAutoAck(false);
  //radio.setRetries(15, 15);
  radio.setPALevel(RF24_PA_MIN);
  radio.openWritingPipe(address);
}

void setup() {
    Serial.begin(9600);
    // Set delay between sensor readings based on sensor details.
    initRadio();
}

void sendMessage(long type, float value){
  Serial.println("sendMessage");
  data.type = type;
  data.value = value;
  if(radio.write(&data, sizeof(Data))){
    Serial.println("Send SUCCESS");
  }else {
    Serial.println("Send FAIL");
  }
  delay(100);
}

void loop() {
  // Delay between measurements.
    delay(1000);

    sendMessage(0, 20.10);
    sendMessage(1, 45.3);
    sendMessage(2, 1033);

}
