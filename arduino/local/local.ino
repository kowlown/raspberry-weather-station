#include <SPI.h>

#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

#include <ArduinoJson.h>

#include <Adafruit_Sensor.h>

#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 3 // broche ou l'on a branche le capteur
#define DHTTYPE DHT22 // DHT 22 (AM2302)
#define RF_CE    7
#define RF_CSN   8

DHT_Unified dht(DHTPIN, DHTTYPE);

//Radio RF24
RF24 radio(RF_CE,RF_CSN);
const byte address[6] = "00001";

//JSON
const int capacity = JSON_OBJECT_SIZE(8);

//Communication structure
struct Data {
  long type;
  float value;
} data;


long previousMillis = 0;
long interval = 3000;  
bool refresh = false;

void displaySensorDetails(sensor_t sensor, String type, String unit)
{
  Serial.println("------------------------------------");
  Serial.println(type);
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(unit);
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(unit);
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(unit);
  Serial.println("------------------------------------");
}

void sendValue(String source, String type, float value) {
  DynamicJsonDocument doc(capacity);
  doc["type_message"] = "measure";
  doc["source_id"] = source;
  doc["value_type"] = type;
  if(isnan(value)) {
    doc["value"] = nullptr;
  } else {
    doc["value"] = value;
  }
  Serial.write(0x11);
  serializeJsonPretty(doc, Serial);
  Serial.write(0x12);
  Serial.println("");
  doc.clear();
}

void initRadio() {
    radio.begin();
    radio.setAutoAck(false);
    //radio.setRetries(15, 15);
    radio.openReadingPipe(0, address);
    radio.setPALevel(RF24_PA_MAX);
    radio.startListening();
}

String convertType(long type){
    if(type == 0) {
        return "temperature";
    } else if(type == 1) {
        return "humidity";
    } else if(type == 2) {
        return "pressure";
    }
}

void readRadio(){
  if(radio.available()){
    while(radio.available()) {
      radio.read(&data, sizeof(data));
      sendValue("remote", convertType(data.type), data.value);
    }
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  dht.begin();

  sensor_t sensor;

  Serial.println("DHTxx Unified Sensor Example");
  // Print temperature sensor details.
  dht.temperature().getSensor(&sensor);
  displaySensorDetails(sensor, "Temperature", "%");

  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  displaySensorDetails(sensor, "Humidity", "%");

  //Init RF24 Radio
  initRadio();
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis > interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;  
  
    
    sensors_event_t event;  
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature)) {
      Serial.println("Error reading temperature!");
    }
    else {
      //Serial.print("Temperature: ");
      //Serial.print(event.temperature);
      //Serial.println(" *C");
      float temperature = event.temperature;
      sendValue("local", "temperature", temperature);
    }
    // Get humidity event and print its value.
    dht.humidity().getEvent(&event);
    if (isnan(event.relative_humidity)) {
      Serial.println("Error reading humidity!");
    }
    else {
      //Serial.print("Humidity: ");
      //Serial.print(event.relative_humidity);
      //Serial.println("%");
      float humidity = event.relative_humidity;
      sendValue("local", "humidity", humidity);
    }
  }
  readRadio();
  
}
