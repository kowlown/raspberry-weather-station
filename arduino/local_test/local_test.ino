#include <SPI.h>

#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

//#include <ArduinoJson.h>

#define RF_CE    7
#define RF_CSN   8

//Radio RF24
RF24 radio(RF_CE,RF_CSN);
const byte address[6] = "00001";

//JSON
//const int capacity = JSON_OBJECT_SIZE(8);

//Communication structure
struct Data {
  long type;
  float value;
} data;


long previousMillis = 0;
long interval = 3000;  
bool refresh = false;


void sendValue(String source, String type, float value) {
/*  DynamicJsonDocument doc(capacity);
  doc["type_message"] = "measure";
  doc["source_id"] = source;
  doc["value_type"] = type;
  doc["value"] = value;
  Serial.write(0x11);
  serializeJsonPretty(doc, Serial);
  Serial.write(0x12);
  Serial.println("");
  doc.clear();*/
}

void initRadio() {
    radio.begin();
    radio.setAutoAck(false);
    //radio.setRetries(15, 15);
    radio.openReadingPipe(0, address);
    radio.setPALevel(RF24_PA_MIN);
    radio.startListening();
}

void readRadio(){
  //if(radio.available()){
    while(radio.available()) {
      radio.read(&data, sizeof(Data));
      Serial.print("type:");
      Serial.print(data.type);
      Serial.print("value:");
      Serial.println(data.value);
      sendValue("remote", "test_radio", data.value);
    }
  //}
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //Init RF24 Radio
  initRadio();
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis > interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;  
    float humidity = 45.60;
    float temperature = 21.30f;
    //radio.stopListening();
    sendValue("local", "humidity", humidity);
    sendValue("local", "temperature", temperature);
    //radio.startListening();
    delay(100);
  }
  readRadio();
  
}
