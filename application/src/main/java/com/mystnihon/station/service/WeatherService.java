package com.mystnihon.station.service;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeatherService {
    private static final Logger log = LoggerFactory.getLogger(WeatherService.class);
    protected final Nitrite nitrite;

    public WeatherService(Nitrite nitrite) {
        this.nitrite = nitrite;
    }

    protected <T> void updateOrInsertCollection(T data, Class<T> tClass) {
        log.debug("replace document for class collection {}", tClass.getSimpleName());
        ObjectRepository<T> repository = nitrite.getRepository(tClass);
        if (tClass.isInstance(data)) {
            repository.update(data, true);
        }
    }
}
