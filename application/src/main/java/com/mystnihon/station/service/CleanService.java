package com.mystnihon.station.service;

import com.mystnihon.station.db.collection.DayForecastCollection;
import com.mystnihon.station.db.collection.DayForecastCollection_;
import com.mystnihon.station.db.collection.ForecastCollection;
import com.mystnihon.station.db.collection.ForecastCollection_;
import com.mystnihon.station.db.collection.WeatherCollection;
import com.mystnihon.station.db.collection.WeatherCollection_;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
public class CleanService extends WeatherService {

    public CleanService(Nitrite nitrite) {
        super(nitrite);
    }

    public void removeOldEntries() {
        ObjectRepository<WeatherCollection> repository2 = nitrite.getRepository(WeatherCollection.class);
        repository2.remove(ObjectFilters.lt(WeatherCollection_.creationDate.getName(), ZonedDateTime.now().minusDays(1)));

        ObjectRepository<ForecastCollection> repository1 = nitrite.getRepository(ForecastCollection.class);
        repository1.remove(ObjectFilters.lt(ForecastCollection_.creationDate.getName(), ZonedDateTime.now().minusDays(1)));

        ObjectRepository<DayForecastCollection> repository3 = nitrite.getRepository(DayForecastCollection.class);
        repository3.remove(ObjectFilters.lt(DayForecastCollection_.creationDate.getName(), ZonedDateTime.now().minusDays(1)));
    }
}
