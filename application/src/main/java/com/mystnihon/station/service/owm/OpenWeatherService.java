package com.mystnihon.station.service.owm;

import com.mystnihon.station.util.Constants;
import com.mystnihon.station.config.ApplicationProperties;
import com.mystnihon.station.db.collection.ForecastCollection;
import com.mystnihon.station.db.collection.ForecastCollection_;
import com.mystnihon.station.db.collection.WeatherCollection;
import com.mystnihon.station.db.collection.WeatherCollection_;
import com.mystnihon.station.rest.network.NetworkAdapter;
import com.mystnihon.station.rest.owm.dto.ForecastData;
import com.mystnihon.station.rest.owm.dto.ListForecast;
import com.mystnihon.station.rest.owm.dto.WeatherData;
import com.mystnihon.station.service.WeatherService;
import com.mystnihon.station.util.PropertyComponent;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.FindOptions;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.SortOrder;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OpenWeatherService extends WeatherService {


    private final NetworkAdapter networkAdapter;
    private final ApplicationProperties applicationProperties;
    private final PropertyComponent propertyComponent;

    public OpenWeatherService(NetworkAdapter networkAdapter, ApplicationProperties applicationProperties, PropertyComponent propertyComponent, Nitrite nitrite) {
        super(nitrite);
        this.networkAdapter = networkAdapter;
        this.applicationProperties = applicationProperties;
        this.propertyComponent = propertyComponent;
    }

    public Observable<ListForecast> forecastData() {
        Optional<String> optional = propertyComponent.getValueFor(PropertyComponent.PROPERTY_CITY);
        String cityCode = optional.orElse(Constants.DEFAULT_CITY_CODE_RENNES_FR);

        ObjectRepository<ForecastCollection> repository = nitrite.getRepository(ForecastCollection.class);
        Cursor<ForecastCollection> collectionCursor = repository.find(
                ObjectFilters.and(
                        ObjectFilters.gt(ForecastCollection_.creationDate.getName(), ZonedDateTime.now().minus(1, ChronoUnit.HOURS)),
                        ObjectFilters.eq(ForecastCollection_.cityCode.getName(), cityCode)
                ),
                FindOptions.sort(ForecastCollection_.creationDate.getName(), SortOrder.Descending));

        if (collectionCursor.size() > 0) {
            log.debug("Return saved data");
            return getDataFromDatabase(repository, cityCode);
        }

        return networkAdapter.getOpenWeatherMapApi().getForecast(optional.orElse(Constants.DEFAULT_CITY_CODE_RENNES_FR), applicationProperties.getOpenWeatherMap().getAppId())
                .map(ForecastData::getListForecast)
                .doOnNext(forecastData -> {
                    log.info("doOnNext forecast");
                    for (ListForecast forecastDatum : forecastData) {
                        replace(forecastDatum, cityCode);
                    }
                }).flatMap(f -> getDataFromDatabase(repository, cityCode));
    }

    private Observable<ListForecast> getDataFromDatabase(ObjectRepository<ForecastCollection> repository, String cityCode) {
        Cursor<ForecastCollection> collectionCursor = repository.find(
                ObjectFilters.and(
                        ObjectFilters.gte(ForecastCollection_.date.getName(), LocalDate.now().plus(1, ChronoUnit.DAYS).atStartOfDay().atOffset(ZoneOffset.UTC)),
                        ObjectFilters.eq(ForecastCollection_.cityCode.getName(), cityCode)
                ),
                FindOptions.sort(ForecastCollection_.date.getName(), SortOrder.Ascending));
        List<ListForecast> listForecasts = collectionCursor.toList().stream().map(ForecastCollection::getForecastData).collect(Collectors.toList());
        return Observable.just(listForecasts).flatMapIterable(l -> l);
    }

    public Observable<WeatherData> weatherData() {
        Optional<String> optional = propertyComponent.getValueFor(PropertyComponent.PROPERTY_CITY);
        String cityCode = optional.orElse(Constants.DEFAULT_CITY_CODE_RENNES_FR);

        ObjectRepository<WeatherCollection> repository = nitrite.getRepository(WeatherCollection.class);

        return Optional.ofNullable(repository.find(
                ObjectFilters.and(
                        ObjectFilters.gte(WeatherCollection_.creationDate.getName(), ZonedDateTime.now().minus(10, ChronoUnit.MINUTES)),
                        ObjectFilters.eq(WeatherCollection_.cityCode.getName(), cityCode)
                ),
                FindOptions.sort(WeatherCollection_.creationDate.getName(), SortOrder.Descending))
                .firstOrDefault())
                .map(WeatherCollection::getWeatherData)
                .map(item -> {
                    log.debug("Return saved data");
                    return Observable.just(item);
                })
                .orElseGet(() -> networkAdapter.getOpenWeatherMapApi().getWeather(cityCode, applicationProperties.getOpenWeatherMap().getAppId()).doOnNext(weatherData -> replace(weatherData, cityCode)));

    }

    private void replace(WeatherData weatherData, String cityCode) {
        log.debug("save weather data");
        ObjectRepository<WeatherCollection> repository = nitrite.getRepository(WeatherCollection.class);
        repository.remove(ObjectFilters.ALL);
        updateOrInsertCollection(new WeatherCollection(weatherData, cityCode), WeatherCollection.class);
    }

    private void replace(ListForecast listForecast, String cityCode) {
        log.debug("save forecast data");
        updateOrInsertCollection(new ForecastCollection(listForecast, cityCode), ForecastCollection.class);
    }

}
