package com.mystnihon.station.service.accuweather;

import com.mystnihon.station.config.ApplicationProperties;
import com.mystnihon.station.db.collection.DayForecastCollection;
import com.mystnihon.station.db.collection.DayForecastCollection_;
import com.mystnihon.station.rest.accuweather.dto.DailyForecast;
import com.mystnihon.station.rest.accuweather.dto.FiveDayForecastData;
import com.mystnihon.station.rest.network.NetworkAdapter;
import com.mystnihon.station.service.WeatherService;
import com.mystnihon.station.service.owm.OpenWeatherService;
import com.mystnihon.station.util.PropertyComponent;
import com.mystnihon.station.util.SameDayObjectFilter;
import io.reactivex.Observable;
import org.dizitart.no2.FindOptions;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.SortOrder;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Optional;

import static com.mystnihon.station.util.Constants.RENNES_LOCATION_KEY;

@Service
public class AccuWeatherService extends WeatherService {
    private static final Logger log = LoggerFactory.getLogger(OpenWeatherService.class);

    private final NetworkAdapter networkAdapter;
    private final ApplicationProperties applicationProperties;
    private final PropertyComponent propertyComponent;

    public AccuWeatherService(NetworkAdapter networkAdapter, ApplicationProperties applicationProperties, Nitrite nitrite, PropertyComponent propertyComponent) {
        super(nitrite);
        this.networkAdapter = networkAdapter;
        this.applicationProperties = applicationProperties;
        this.propertyComponent = propertyComponent;
    }

    public Observable<DailyForecast> getForecastForDay(LocalDate localDate) {
        String locationKey = propertyComponent.getValueFor(PropertyComponent.PROPERTY_LOCATION_KEY).orElse(RENNES_LOCATION_KEY);

        ObjectRepository<DayForecastCollection> repository = nitrite.getRepository(DayForecastCollection.class);

        Cursor<DayForecastCollection> collectionCursor = repository.find(
                ObjectFilters.and(
                        ObjectFilters.gt(DayForecastCollection_.creationDate.getName(), ZonedDateTime.now().minus(1, ChronoUnit.HOURS)),
                        ObjectFilters.eq(DayForecastCollection_.locationKey.getName(), locationKey)
                ),
                FindOptions.sort(DayForecastCollection_.creationDate.getName(), SortOrder.Descending));

        if (collectionCursor.size() <= 0) {
            return networkAdapter.getAccuWeatherApi().get5DayForecast(locationKey, applicationProperties.getAccuWeather().getApiKey()).cache()
                    .flatMapIterable(FiveDayForecastData::getDailyForecasts)
                    .doOnNext(dailyForecast -> {
                        log.debug("Save on next");
                        updateOrInsertCollection(new DayForecastCollection(dailyForecast, locationKey), DayForecastCollection.class);
                    }).collect(ArrayList::new, ArrayList::add).toObservable().flatMap(p -> getDailyForecastObservable(localDate, repository));
        } else {
            return getDailyForecastObservable(localDate, repository);
        }


    }

    private Observable<DailyForecast> getDailyForecastObservable(LocalDate localDate, ObjectRepository<DayForecastCollection> repository) {
        String locationKey = propertyComponent.getValueFor(PropertyComponent.PROPERTY_LOCATION_KEY).orElse(RENNES_LOCATION_KEY);
        DayForecastCollection dayForecastCollection = repository.find(ObjectFilters.and(
                ObjectFilters.eq(DayForecastCollection_.locationKey.getName(), locationKey),

                SameDayObjectFilter.sameDay(DayForecastCollection_.forecastData_date.getName(), localDate))
        ).firstOrDefault();
        return Optional.ofNullable(dayForecastCollection).map(DayForecastCollection::getForecastData).map(Observable::just).orElse(Observable.empty());
    }


}
