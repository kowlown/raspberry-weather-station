package com.mystnihon.station.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import com.mystnihon.station.util.PropertyComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SerialManager {
    public static final int CH_END = 0x12;
    public static final int CH_START = 0x11;
    public static final String PROPERTY_PORT = "port";
    private final PropertyComponent propertyComponent;
    private SerialMessageListener serialMessageListener;

    public SerialManager(PropertyComponent propertyComponent) {
        this.propertyComponent = propertyComponent;
    }

    public PropertyComponent getPropertyComponent() {
        return propertyComponent;
    }

    public void setSerialMessageListener(SerialMessageListener serialMessageListener) {
        this.serialMessageListener = serialMessageListener;
    }

    public List<String> getPortList() {
        return Arrays.stream(SerialPort.getCommPorts()).map(SerialPort::getSystemPortName).collect(Collectors.toList());
    }

    public void openDefault() {
        Arrays.stream(SerialPort.getCommPorts()).forEach(serialPort -> log.debug("Serial port:" + serialPort.getSystemPortName()));
        Optional<SerialPort> optionalSerialPort = Arrays.stream(SerialPort.getCommPorts()).filter(s -> s.getSystemPortName().equalsIgnoreCase(getSelectedPort())).findFirst();
        optionalSerialPort.ifPresent(serialPort -> {
            if (!serialPort.isOpen()) {
                serialPort.openPort();
            } else {
                if (serialPort.closePort()) {
                    serialPort.openPort(400);
                }
            }
            serialPort.addDataListener(new SerialPortMessageListener() {
                @Override
                public byte[] getMessageDelimiter() {
                    return new byte[]{CH_END};
                }

                @Override
                public boolean delimiterIndicatesEndOfMessage() {
                    return true;
                }

                @Override
                public int getListeningEvents() {
                    return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
                }

                @Override
                public void serialEvent(SerialPortEvent event) {
                    byte[] delimitedMessage = event.getReceivedData();
                    String message = new String(delimitedMessage);

                    int indexOfStartPayload = message.indexOf(CH_START);
                    if (indexOfStartPayload >= 0) {
                        int start = indexOfStartPayload + 1;
                        int end = message.indexOf(CH_END);
                        char[] chars = new char[end - start];

                        message.getChars(start, end, chars, 0);
                        messageReceived(new String(chars));
                    }
                }
            });

        });

    }

    private String getSelectedPort() {
        return propertyComponent.getValueFor(PROPERTY_PORT).orElse(null);
    }

    private void messageReceived(String message) {
        Optional.ofNullable(serialMessageListener).ifPresent(listener -> listener.onMessageReceived(message));
    }
}
