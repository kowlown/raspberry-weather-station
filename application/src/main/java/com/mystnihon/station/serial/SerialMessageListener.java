package com.mystnihon.station.serial;

public interface SerialMessageListener {
    void onMessageReceived(String message);
}
