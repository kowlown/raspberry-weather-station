package com.mystnihon.station;

import com.mystnihon.station.config.ApplicationProperties;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import static javafx.application.Application.launch;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ApplicationProperties.class)
public class StartApplication {

    public static void main(String[] args) {
        launch(WeatherApplication.class, args);
    }

}
