package com.mystnihon.station.jfx.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mystnihon.station.jfx.controllers.component.ForecastBlock;
import com.mystnihon.station.jfx.controllers.component.Message;
import com.mystnihon.station.rest.owm.dto.ListForecast;
import com.mystnihon.station.rest.owm.dto.Weather;
import com.mystnihon.station.rest.owm.dto.WeatherData;
import com.mystnihon.station.serial.SerialManager;
import com.mystnihon.station.serial.SerialMessageListener;
import com.mystnihon.station.service.CleanService;
import com.mystnihon.station.service.accuweather.AccuWeatherService;
import com.mystnihon.station.service.owm.OpenWeatherService;
import com.mystnihon.station.util.IconUtil;
import com.mystnihon.station.util.ResourceConstantName;
import com.mystnihon.station.util.TempConverter;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.swing.Timer;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import static com.mystnihon.station.rest.owm.api.OpenWeatherMapApi.OPEN_WEATHER_MAP_ICON_URL;

@Slf4j
@Component
public class MainController implements Initializable, SerialMessageListener {

    private static final String TYPE_TEMPERATURE = "temperature";
    private static final String TYPE_HUMIDITY = "humidity";
    private static final String TYPE_PRESSURE = "pressure";
    private static final String SOURCE_LOCAL = "local";
    private static final String SOURCE_REMOTE = "remote";
    private static final int MAX_FORECAST_TO_DISPLAY = 3;
    private static final NumberFormat tempFormatter = new DecimalFormat("#");
    private static final NumberFormat valueFormatter = DecimalFormat.getInstance(Locale.getDefault());
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private static final DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern("EEEE");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    private static final String CELSIUS_SUFFIX = "°C";
    private static final String PERCENTAGE_SUFFIX = "%";
    private static final String H_PA_SUFFIX = "hPa";
    private static final int DELAY = 1000;

    @FXML
    private Label labelTemperature;
    @FXML
    private Label labelHumidity;
    @FXML
    private Label labelPressure;
    @FXML
    private Label labelTemperatureLocal;
    @FXML
    private Label labelHumidityLocal;
    @FXML
    private Label labelTime;
    @FXML
    private Label labelDay;
    @FXML
    private Label labelDate;
    @FXML
    private ImageView imageViewCurrentWeather;
    @FXML
    private Label labelWeatherCurrent;
    @FXML
    private HBox forecastList;
    @FXML
    private Label labelCity;
    @FXML
    private Label labelTemp;

    private Gson gson;
    private final ResourceBundle weatherStringCode;
    private final ConfigurableApplicationContext configurableApplicationContext;
    private final OpenWeatherService openWeatherService;
    private final AccuWeatherService accuWeatherService;
    private final SerialManager serialManager;
    private final CleanService cleanService;
    private Disposable subscribeWeather;
    private Disposable subscribeForecast;

    public MainController(ConfigurableApplicationContext configurableApplicationContext, OpenWeatherService openWeatherService, AccuWeatherService accuWeatherService, SerialManager serialManager, CleanService cleanService) {
        this.configurableApplicationContext = configurableApplicationContext;
        this.openWeatherService = openWeatherService;
        this.accuWeatherService = accuWeatherService;
        this.serialManager = serialManager;
        this.cleanService = cleanService;
        gson = new GsonBuilder().create();
        weatherStringCode = ResourceBundle.getBundle(ResourceConstantName.RESOURCE_WEATHER_CODE, Locale.getDefault());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Timer timer = new Timer(DELAY, e -> {
            LocalDateTime now = LocalDateTime.now();
            Platform.runLater(() -> {
                labelDate.setText(dateFormatter.format(now));
                labelDay.setText(dayFormatter.format(now));
                labelTime.setText(timeFormatter.format(now));
            });
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.start();
        serialManager.setSerialMessageListener(this);
        serialManager.openDefault();
        refresh();
    }

    private void refresh() {
        cleanPassedData();
        updateWeatherData();
        updateForecastData();
    }

    @Scheduled(cron = "${updateWeatherData:0 0/10 * * * ?}")
    public void updateWeatherData() {
        subscribeWeather = openWeatherService.weatherData()
                .observeOn(JavaFxScheduler.platform())
                .subscribe(this::updateCurrentWeatherUI, throwable -> log.error("Error", throwable));
    }


    @Scheduled(cron = "${updateForecastData:0 0 * * * ?}")
    public void updateForecastData() {
        Platform.runLater(() -> forecastList.getChildren().clear());
        subscribeForecast = openWeatherService.forecastData()
                .filter(this::filterOnlyForNoonForecast)
                .take(MAX_FORECAST_TO_DISPLAY)
                .concatMap(this::completeWithAccuWeatherData)
                .observeOn(JavaFxScheduler.platform())
                .doOnTerminate(() -> log.info("onTerminate"))
                .subscribe(forecastBlock -> {
                    log.info("Date {}", forecastBlock.getDate());
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/scenes/small_forecast.fxml"));
                    fxmlLoader.setController(new SmallForecastController(forecastBlock));
                    forecastList.getChildren().add(fxmlLoader.load());
                }, throwable -> log.error("Error", throwable), () -> log.info("onComplete"));
    }

    @Scheduled(cron = "${cleanPassedData:0 0 0 * * ?}")
    public void cleanPassedData() {
        cleanService.removeOldEntries();
    }


    private Observable<ForecastBlock> completeWithAccuWeatherData(ListForecast listForecast) {
        return accuWeatherService.getForecastForDay(
                LocalDateTime.ofEpochSecond(
                        listForecast.getDt(), 0, ZoneOffset.UTC
                ).toLocalDate())
                .zipWith(Observable.just(listForecast), (dailyForecast, listForecast1) -> new ForecastBlock(listForecast1, dailyForecast));
    }

    private boolean filterOnlyForNoonForecast(ListForecast listForecast) {
        return LocalDateTime.ofEpochSecond(listForecast.getDt(), 0, ZoneOffset.UTC).toLocalTime().equals(LocalTime.NOON);
    }

    private void updateCurrentWeatherUI(WeatherData weatherData) {
        List<Weather> weathers = weatherData.getWeather();
        labelCity.setText(weatherData.getName());
        labelTemp.setText(TempConverter.convertKelvinToCelsius(weatherData.getMain().getTemp(), tempFormatter, CELSIUS_SUFFIX));
        weathers.stream().findFirst().ifPresent(weather -> {
            labelWeatherCurrent.setText(getWeatherLabelForCode(weather.getId()));
            String icon = weather.getIcon();
            String imageUrl = String.format(OPEN_WEATHER_MAP_ICON_URL, icon);
            imageViewCurrentWeather.setImage(new Image(imageUrl));
        });

    }

    public String getWeatherLabelForCode(int code) {
        return weatherStringCode.getString(String.valueOf(code));
    }

    @PreDestroy
    public void onDestroy() {
        Optional.ofNullable(subscribeWeather).ifPresent(Disposable::dispose);
        Optional.ofNullable(subscribeForecast).ifPresent(Disposable::dispose);
    }

    @Override
    public void onMessageReceived(String jsonMessage) {
        Message message = gson.fromJson(jsonMessage, Message.class);
        if (SOURCE_LOCAL.equalsIgnoreCase(message.getSourceId())) {
            manageMessageToDisplay(message, labelTemperatureLocal, labelHumidityLocal, null);
        } else if (SOURCE_REMOTE.equalsIgnoreCase(message.getSourceId())) {
            manageMessageToDisplay(message, labelTemperature, labelHumidity, labelPressure);
        }
    }

    private void manageMessageToDisplay(Message message, Label labelTemperature, Label labelHumidity, Label labelPressure) {
        switch (message.getValueType()) {
            case TYPE_TEMPERATURE:
                displayMessage(message.getValue(), labelTemperature, CELSIUS_SUFFIX);
                break;
            case TYPE_HUMIDITY:
                displayMessage(message.getValue(), labelHumidity, PERCENTAGE_SUFFIX);
                break;
            case TYPE_PRESSURE:
                Optional.of(labelPressure).ifPresent(label -> displayMessage(message.getValue(), label, H_PA_SUFFIX));
                break;
            default:
                break;
        }
    }

    private void displayMessage(float value, Label label, String suffix) {
        Platform.runLater(() -> label.setText(valueFormatter.format(value).concat(suffix)));
    }

    @SuppressWarnings("unused")
    @FXML
    public void onHandleMenuItemSettings(ActionEvent actionEvent) {
        ResourceBundle bundle = ResourceBundle.getBundle(ResourceConstantName.RESOURCE_BUNDLE, Locale.getDefault());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/scenes/pref_screen.fxml"), bundle);
        fxmlLoader.setControllerFactory(configurableApplicationContext::getBean);
        Parent root;
        try {
            root = fxmlLoader.load();
            Stage stage = new Stage();
            IconUtil.setIcon(stage);
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            log.error("Error while loading fxml file", e);
        }

    }

    @SuppressWarnings("unused")
    @FXML
    public void onHandleMenuItemExit(ActionEvent actionEvent) {
        System.exit(0);
    }
}
