package com.mystnihon.station.jfx.controllers.component;

import com.mystnihon.station.rest.accuweather.dto.DailyForecast;
import com.mystnihon.station.rest.owm.dto.ListForecast;
import com.mystnihon.station.rest.owm.dto.Weather;
import com.mystnihon.station.util.ResourceConstantName;
import javafx.scene.image.Image;
import lombok.Data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.mystnihon.station.rest.owm.api.OpenWeatherMapApi.OPEN_WEATHER_MAP_ICON_URL;

@Data
public class ForecastBlock {
    private static final NumberFormat valueFormatter = new DecimalFormat("#");
    private static final DateTimeFormatter forecastFormatter = DateTimeFormatter.ofPattern("EE dd/MM");
    private static final ResourceBundle resumeResource = ResourceBundle.getBundle(ResourceConstantName.RESOURCE_WEATHER_CODE_SMALL, Locale.getDefault());


    private Image image;
    private String label;
    private String date;
    private String moreInfo;
    private String minTemp;
    private String maxTemp;

    public ForecastBlock(ListForecast forecast, DailyForecast dailyForecast) {

        Weather weather = forecast.getWeather().get(0);
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(forecast.getDt(), 0, ZoneOffset.UTC);
        setDate(localDateTime.format(forecastFormatter));
        setLabel(resumeResource.getString(String.valueOf(weather.getId())));//convert.apply(weather.getId());
        setMoreInfo(String.format("%s°C - %s°C", valueFormatter.format(dailyForecast.getTemperature().getMinimum().getValue()), valueFormatter.format(dailyForecast.getTemperature().getMaximum().getValue())));
        setMinTemp(String.format("%s°", valueFormatter.format(dailyForecast.getTemperature().getMinimum().getValue())));
        setMaxTemp(String.format("%s°", valueFormatter.format(dailyForecast.getTemperature().getMaximum().getValue())));
        setImage(new Image(String.format(OPEN_WEATHER_MAP_ICON_URL, weather.getIcon())));
    }

}
