package com.mystnihon.station.jfx.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mystnihon.station.config.ApplicationProperties;
import com.mystnihon.station.rest.accuweather.dto.autocomplete.AutoCompleteResponse;
import com.mystnihon.station.rest.network.NetworkAdapter;
import com.mystnihon.station.serial.SerialManager;
import com.mystnihon.station.util.PropertyComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.controlsfx.control.textfield.TextFields;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Collections;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class SettingsController implements Initializable {

    private final Gson gson;
    @FXML
    private ComboBox<String> comboBoxPort;

    @FXML
    private TextField textFieldCityCodeAccu;

    @FXML
    private TextField textFieldCityCodeOwm;

    private final SerialManager serialManager;

    private final PropertyComponent propertyComponent;

    private final NetworkAdapter networkAdapter;
    private final ApplicationProperties applicationProperties;
    private AutoCompleteResponse selected;

    public SettingsController(SerialManager serialManager, PropertyComponent propertyComponent, NetworkAdapter networkAdapter, ApplicationProperties applicationProperties) {
        this.serialManager = serialManager;
        this.propertyComponent = propertyComponent;
        this.networkAdapter = networkAdapter;
        this.applicationProperties = applicationProperties;
        this.gson = new GsonBuilder().create();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> stringObservableList = FXCollections.observableArrayList(serialManager.getPortList());
        comboBoxPort.setItems(stringObservableList);
        serialManager.getPropertyComponent().getValueFor(SerialManager.PROPERTY_PORT).ifPresent(portName -> comboBoxPort.getSelectionModel().select(portName));
        propertyComponent.getValueFor(PropertyComponent.PROPERTY_CITY).ifPresent(textFieldCityCodeOwm::setText);
        propertyComponent.getValueFor(PropertyComponent.DATA).ifPresent(string -> {
            fill(gson.fromJson(string, AutoCompleteResponse.class));
        });
        TextFields.bindAutoCompletion(textFieldCityCodeAccu, param -> {
            String userText = param.getUserText();
            if (userText.length() >= 3) {
                return networkAdapter.getAccuWeatherApi().autoCompleteLocation(userText, applicationProperties.getAccuWeather().getApiKey())
                        .debounce(400, TimeUnit.MILLISECONDS).blockingFirst();
            }
            return Collections.emptyList();
        }, new StringConverter<>() {
            @Override
            public String toString(AutoCompleteResponse object) {
                return object.getLocalizedName();
            }

            @Override
            public AutoCompleteResponse fromString(String string) {
                return null;
            }
        }).setOnAutoCompleted(event -> {
            AutoCompleteResponse completion = event.getCompletion();
            selected = completion;
            textFieldCityCodeOwm.setText(completion.getLocalizedName() + "," + completion.getCountry().getID());
            log.info("handle completed, {}", completion);
        });
    }

    private void fill(AutoCompleteResponse fromJson) {
        selected = fromJson;
        textFieldCityCodeAccu.setText(fromJson.getLocalizedName());
    }

    @FXML
    public void onHandleActionSave(ActionEvent actionEvent) {
        Optional.ofNullable(comboBoxPort.getSelectionModel()).map(SelectionModel::getSelectedItem).ifPresent(portName -> {
            serialManager.getPropertyComponent().setValueFor(SerialManager.PROPERTY_PORT, portName);
            serialManager.openDefault();
        });

        if (StringUtils.isNotBlank(textFieldCityCodeOwm.getText())) {
            propertyComponent.setValueFor(PropertyComponent.PROPERTY_CITY, textFieldCityCodeOwm.getText());
        }
        Optional.ofNullable(selected).ifPresent(autoCompleteResponse -> {
            propertyComponent.setValueFor(PropertyComponent.PROPERTY_LOCATION_KEY, autoCompleteResponse.getKey());
            propertyComponent.setValueFor(PropertyComponent.DATA, gson.toJson(autoCompleteResponse));
        });
        ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
    }
}
