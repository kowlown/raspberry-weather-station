package com.mystnihon.station.jfx.controllers.component;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Message {

    @SerializedName("type_message")
    @Expose
    private String typeMessage;
    @SerializedName("source_id")
    @Expose
    private String sourceId;
    @SerializedName("value_type")
    @Expose
    private String valueType;
    @SerializedName("value")
    @Expose
    private float value;

}
