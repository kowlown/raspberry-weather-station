package com.mystnihon.station.jfx.controllers;

import com.mystnihon.station.jfx.controllers.component.ForecastBlock;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class SmallForecastController implements Initializable {
    @FXML
    ImageView labelSmallWeatherImage;
    @FXML
    Label labelSmallWeatherDay;
    @FXML
    Label labelSmallWeather;
    @FXML
    Label labelSmallWeatherMinMax;
    @FXML
    Label labelMinTemp;
    @FXML
    Label labelMaxTemp;

    private ForecastBlock forecastBlock;

    public SmallForecastController(ForecastBlock forecastBlock) {
        this.forecastBlock = forecastBlock;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        labelSmallWeather.setText(forecastBlock.getLabel());
        labelSmallWeatherImage.setImage(forecastBlock.getImage());
        labelSmallWeatherDay.setText(forecastBlock.getDate());
        labelSmallWeatherMinMax.setText(forecastBlock.getMoreInfo());
//        labelMinTemp.setText(forecastBlock.getMinTemp());
//        labelMaxTemp.setText(forecastBlock.getMaxTemp());
    }
}
