package com.mystnihon.station.db.collection;

import com.mystnihon.nitrite.annotations.Collection;
import com.mystnihon.station.rest.owm.dto.ListForecast;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dizitart.no2.objects.Id;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Data
@Collection
@NoArgsConstructor
public class ForecastCollection implements Serializable {

    @Id
    private int documentId;

    private ZonedDateTime creationDate;
    private OffsetDateTime date;
    private ListForecast forecastData;
    private String cityCode;


    public ForecastCollection(ListForecast forecastData, String cityCode) {
        this.cityCode = cityCode;
        this.documentId = forecastData.getDt();
        this.date = LocalDateTime.ofEpochSecond(forecastData.getDt(), 0, ZoneOffset.UTC).atOffset(ZoneOffset.UTC);
        this.creationDate = ZonedDateTime.now();
        this.forecastData = forecastData;
    }

}
