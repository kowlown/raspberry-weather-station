package com.mystnihon.station.db.collection;

import com.mystnihon.nitrite.annotations.Collection;
import com.mystnihon.station.rest.owm.dto.WeatherData;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dizitart.no2.objects.Id;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Data
@Collection
@NoArgsConstructor
public class WeatherCollection implements Serializable {
    @Id
    private int documentId;

    private ZonedDateTime creationDate;

    private WeatherData weatherData;
    private String cityCode;


    public WeatherCollection(WeatherData weatherData, String cityCode) {
        this.cityCode = cityCode;
        this.documentId = weatherData.getDt();
        this.weatherData = weatherData;
        this.creationDate = ZonedDateTime.now();
    }

}
