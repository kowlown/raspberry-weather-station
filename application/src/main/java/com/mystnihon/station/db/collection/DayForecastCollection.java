package com.mystnihon.station.db.collection;

import com.mystnihon.nitrite.annotations.Collection;
import com.mystnihon.station.rest.accuweather.dto.DailyForecast;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dizitart.no2.objects.Id;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Data
@Collection
@NoArgsConstructor
public class DayForecastCollection implements Serializable {
    @Id
    private int documentId;

    private ZonedDateTime creationDate;

    private DailyForecast forecastData;
    private String locationKey;


    public DayForecastCollection(DailyForecast forecastData, String locationKey) {
        this.locationKey = locationKey;
        this.documentId = forecastData.getEpochDate();
        this.creationDate = ZonedDateTime.now();
        this.forecastData = forecastData;
    }

}
