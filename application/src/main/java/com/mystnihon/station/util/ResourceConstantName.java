package com.mystnihon.station.util;

public class ResourceConstantName {

    public static final String RESOURCE_BUNDLE = "strings";
    public static final String RESOURCE_WEATHER_CODE = "weather_code_translation";
    public static final String RESOURCE_WEATHER_CODE_SMALL = "weather_code_resume";

}
