package com.mystnihon.station.util;

import org.dizitart.no2.Document;
import org.dizitart.no2.Filter;
import org.dizitart.no2.NitriteId;
import org.dizitart.no2.filters.BaseFilter;
import org.dizitart.no2.mapper.NitriteMapper;
import org.dizitart.no2.objects.filters.BaseObjectFilter;
import org.dizitart.no2.store.NitriteMap;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoZonedDateTime;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.dizitart.no2.util.DocumentUtils.getFieldValue;

public class SameDayObjectFilter extends BaseObjectFilter {
    protected NitriteMapper nitriteMapper;


    @Override
    public void setNitriteMapper(NitriteMapper nitriteMapper) {
        this.nitriteMapper = nitriteMapper;
    }

    private String field;
    private LocalDate value;

    SameDayObjectFilter(String field, LocalDate value) {
        this.field = field;
        this.value = value;
    }

    @Override
    public Set<NitriteId> apply(NitriteMap<NitriteId, Document> documentMap) {
        Filter eqFilter;
        eqFilter = new SameDayFilter(field, value);

        return eqFilter.apply(documentMap);
    }

    public static SameDayObjectFilter sameDay(String field, LocalDate date) {
        return new SameDayObjectFilter(field, date);
    }


    public static class SameDayFilter extends BaseFilter {
        private String field;
        private LocalDate value;

        SameDayFilter(String field, LocalDate value) {
            this.field = field;
            this.value = value;
        }

        @Override
        public Set<NitriteId> apply(NitriteMap<NitriteId, Document> documentMap) {
            return matchedSet(documentMap);
        }

        @SuppressWarnings("unchecked")
        private Set<NitriteId> matchedSet(NitriteMap<NitriteId, Document> documentMap) {
            Set<NitriteId> nitriteIdSet = new LinkedHashSet<>();
            for (Map.Entry<NitriteId, Document> entry : documentMap.entrySet()) {
                Document document = entry.getValue();
                Object fieldValue = getFieldValue(document, field);
                if (fieldValue instanceof ChronoLocalDate) {
                    if (((ChronoLocalDate) fieldValue).isEqual(value)) {
                        nitriteIdSet.add(entry.getKey());
                    }
                } else if (fieldValue instanceof ChronoZonedDateTime) {
                    if (((ChronoZonedDateTime<LocalDate>) fieldValue).toLocalDate().isEqual(value)) {
                        nitriteIdSet.add(entry.getKey());
                    }
                } else if (fieldValue instanceof OffsetDateTime) {
                    if (((OffsetDateTime) fieldValue).toLocalDate().isEqual(value)) {
                        nitriteIdSet.add(entry.getKey());
                    }
                } else if (fieldValue instanceof BigDecimal) {
                    if (LocalDateTime.ofEpochSecond(((BigDecimal) fieldValue).longValue(), 0, ZoneOffset.UTC).toLocalDate().isEqual(value)) {
                        nitriteIdSet.add(entry.getKey());
                    }
                }
            }
            return nitriteIdSet;
        }
    }
}
