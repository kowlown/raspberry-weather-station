package com.mystnihon.station.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

@Component
public class PropertyComponent {
    public static final String PROPERTY_CITY = "city";
    public static final String PROPERTY_LOCATION_KEY = "locationKey";
    public static final String DATA = "data";

    private static final Logger log = LoggerFactory.getLogger(PropertyComponent.class);

    private static final String FILE_PROPERTIES = "properties.prop";
    private static final String COMMENTS = "Application properties";
    private Properties properties;

    public Optional<File> getLastDirectoryFor(String key) {
        open(false);
        return Optional.ofNullable(properties.getProperty(key)).map(File::new);
    }

    public void setLastDirectoryFor(String key, File file) {
        open(true);
        properties.put(key, file.getAbsolutePath());
        write();
    }

    public Optional<String> getValueFor(String key) {
        open(false);
        return Optional.ofNullable(properties.getProperty(key));
    }

    public void setValueFor(String key, String value) {
        open(true);
        properties.put(key, value);
        write();
    }

    public void open(boolean force) {
        if (properties == null || force) {
            properties = new Properties();
            try {
                properties.load(new FileInputStream(new File(FILE_PROPERTIES)));
            } catch (IOException e) {
                log.error("Reading properties", e);
            }
        }
    }

    private void write() {
        try {
            properties.store(new FileOutputStream(FILE_PROPERTIES), COMMENTS);
        } catch (IOException e) {
            log.error("IO read/write properties", e);
        }
    }
}
