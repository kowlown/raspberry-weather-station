package com.mystnihon.station.util;

import java.text.NumberFormat;
import java.util.Optional;

public class TempConverter {
    private static final double KELVIN_CONSTANT = 273.15;

    public static String convertKelvinToCelsius(double kelvinTemp, NumberFormat numberFormat, String unitSuffix) {
        return Optional.ofNullable(numberFormat).map(f -> f.format(kelvinTemp - KELVIN_CONSTANT).concat(unitSuffix)).orElse(null);
    }
}
