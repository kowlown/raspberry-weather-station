package com.mystnihon.station.rest.owm.api;

import com.mystnihon.station.rest.owm.dto.ForecastData;
import com.mystnihon.station.rest.owm.dto.WeatherData;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OpenWeatherMapApi {
    String OPEN_WEATHER_MAP_ICON_URL = "http://openweathermap.org/img/wn/%s@2x.png";
    String OPEN_WEATHER_MAP_FORECAST_PATH = "/data/2.5/forecast";
    String OPEN_WEATHER_MAP_WEATHER_PATH = "/data/2.5/weather";

    @GET(OPEN_WEATHER_MAP_FORECAST_PATH)
    Observable<ForecastData> getForecast(@Query("q") String cityCode, @Query("appid") String appId);

    @GET(OPEN_WEATHER_MAP_WEATHER_PATH)
    Observable<WeatherData> getWeather(@Query("q") String cityCode, @Query("appid") String appId);

}
