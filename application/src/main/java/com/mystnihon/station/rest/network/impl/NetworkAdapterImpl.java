package com.mystnihon.station.rest.network.impl;


import com.google.gson.GsonBuilder;
import com.mystnihon.station.config.ApplicationProperties;
import com.mystnihon.station.rest.accuweather.api.AccuWeatherApi;
import com.mystnihon.station.rest.gson.adapters.OffsetDateTimeTypeAdapter;
import com.mystnihon.station.rest.network.NetworkAdapter;
import com.mystnihon.station.rest.owm.api.OpenWeatherMapApi;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.time.OffsetDateTime;
import java.util.concurrent.TimeUnit;


/**
 * Default template
 * Created by Lévi LIRVAT on 25/11/2014.
 */
@Profile("!mock")
@Component
public class NetworkAdapterImpl implements NetworkAdapter {
    private static final Logger log = LoggerFactory.getLogger(NetworkAdapterImpl.class);

    private static final OffsetDateTimeTypeAdapter offsetDateTimeTypeAdapter = new OffsetDateTimeTypeAdapter();
    private static final String CACHE_DIRECTORY = "./cache";
    private final OpenWeatherMapApi openWeatherMapApi;
    private final AccuWeatherApi accuWeatherApi;

    public NetworkAdapterImpl(ApplicationProperties applicationProperties) {
        try {
            FileUtils.forceMkdir(new File(CACHE_DIRECTORY));
        } catch (IOException e) {
            log.error("Could not create cache dir");
        }

        openWeatherMapApi = createService(OpenWeatherMapApi.class, applicationProperties.getOpenWeatherMap().getBaseUrl());
        accuWeatherApi = createService(AccuWeatherApi.class, applicationProperties.getAccuWeather().getBaseUrl());
    }

    @Override
    public OpenWeatherMapApi getOpenWeatherMapApi() {
        return openWeatherMapApi;
    }

    @Override
    public AccuWeatherApi getAccuWeatherApi() {
        return accuWeatherApi;
    }

    private static <T> T createService(Class<T> tClass, String baseUrl) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        gsonBuilder.registerTypeAdapter(OffsetDateTime.class, offsetDateTimeTypeAdapter);
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(log::info).setLevel(HttpLoggingInterceptor.Level.BODY);

        Cache cache = new Cache(new File(CACHE_DIRECTORY), 30 * 1024 * 1024);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(chain -> {
                    Response response = chain.proceed(chain.request());
                    if (response.cacheControl() != null) {
                        // from cache
                        log.debug("From cache");
                    } else if (response.networkResponse() != null) {
                        // from network
                        log.debug("From network");
                    }
                    return response;
                })
                .cache(cache)
                .build();

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build();

        return restAdapter.create(tClass);
    }


}
