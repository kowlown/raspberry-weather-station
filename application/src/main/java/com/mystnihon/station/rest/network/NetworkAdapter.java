package com.mystnihon.station.rest.network;

import com.mystnihon.station.rest.accuweather.api.AccuWeatherApi;
import com.mystnihon.station.rest.owm.api.OpenWeatherMapApi;

public interface NetworkAdapter {
    OpenWeatherMapApi getOpenWeatherMapApi();

    AccuWeatherApi getAccuWeatherApi();
}
