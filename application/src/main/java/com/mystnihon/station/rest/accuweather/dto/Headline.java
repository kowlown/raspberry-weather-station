
package com.mystnihon.station.rest.accuweather.dto;

import java.io.Serializable;
import java.time.OffsetDateTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Headline implements Serializable
{

    @SerializedName("EffectiveDate")
    @Expose
    private OffsetDateTime effectiveDate;
    @SerializedName("EffectiveEpochDate")
    @Expose
    private int effectiveEpochDate;
    @SerializedName("Severity")
    @Expose
    private int severity;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("EndDate")
    @Expose
    private OffsetDateTime endDate;
    @SerializedName("EndEpochDate")
    @Expose
    private int endEpochDate;
    @SerializedName("MobileLink")
    @Expose
    private String mobileLink;
    @SerializedName("Link")
    @Expose
    private String link;
    private final static long serialVersionUID = 6559052885032312600L;

    public OffsetDateTime getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(OffsetDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public int getEffectiveEpochDate() {
        return effectiveEpochDate;
    }

    public void setEffectiveEpochDate(int effectiveEpochDate) {
        this.effectiveEpochDate = effectiveEpochDate;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }

    public int getEndEpochDate() {
        return endEpochDate;
    }

    public void setEndEpochDate(int endEpochDate) {
        this.endEpochDate = endEpochDate;
    }

    public String getMobileLink() {
        return mobileLink;
    }

    public void setMobileLink(String mobileLink) {
        this.mobileLink = mobileLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
