package com.mystnihon.station.rest.network.impl.mock;

public class Constants {
    public static final String WEATHER_JSON = "{\n" +
            "\t\"coord\": {\n" +
            "\t\t\"lon\": -1.67,\n" +
            "\t\t\"lat\": 48.17\n" +
            "\t},\n" +
            "\t\"weather\": [\n" +
            "\t\t{\n" +
            "\t\t\t\"id\": 701,\n" +
            "\t\t\t\"main\": \"Mist\",\n" +
            "\t\t\t\"description\": \"mist\",\n" +
            "\t\t\t\"icon\": \"50d\"\n" +
            "\t\t}\n" +
            "\t],\n" +
            "\t\"base\": \"stations\",\n" +
            "\t\"main\": {\n" +
            "\t\t\"temp\": 275.15,\n" +
            "\t\t\"feels_like\": 271.66,\n" +
            "\t\t\"temp_min\": 274.15,\n" +
            "\t\t\"temp_max\": 276.48,\n" +
            "\t\t\"pressure\": 1021,\n" +
            "\t\t\"humidity\": 100\n" +
            "\t},\n" +
            "\t\"visibility\": 1000,\n" +
            "\t\"wind\": {\n" +
            "\t\t\"speed\": 2.6,\n" +
            "\t\t\"deg\": 70\n" +
            "\t},\n" +
            "\t\"clouds\": {\n" +
            "\t\t\"all\": 90\n" +
            "\t},\n" +
            "\t\"dt\": 1579876650,\n" +
            "\t\"sys\": {\n" +
            "\t\t\"type\": 1,\n" +
            "\t\t\"id\": 6565,\n" +
            "\t\t\"country\": \"FR\",\n" +
            "\t\t\"sunrise\": 1579851903,\n" +
            "\t\t\"sunset\": 1579884711\n" +
            "\t},\n" +
            "\t\"timezone\": 3600,\n" +
            "\t\"id\": 2983989,\n" +
            "\t\"name\": \"Arrondissement de Rennes\",\n" +
            "\t\"cod\": 200\n" +
            "}";
    public static final String FORECAST_JSON = "{\n" +
            "\t\"cod\": \"200\",\n" +
            "\t\"message\": 0,\n" +
            "\t\"cnt\": 40,\n" +
            "\t\"list\": [\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579878000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.13,\n" +
            "\t\t\t\t\"feels_like\": 274.3,\n" +
            "\t\t\t\t\"temp_min\": 277.13,\n" +
            "\t\t\t\t\"temp_max\": 280.65,\n" +
            "\t\t\t\t\"pressure\": 1020,\n" +
            "\t\t\t\t\"sea_level\": 1020,\n" +
            "\t\t\t\t\"grnd_level\": 1012,\n" +
            "\t\t\t\t\"humidity\": 84,\n" +
            "\t\t\t\t\"temp_kf\": -3.52\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 82\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 1.54,\n" +
            "\t\t\t\t\"deg\": 94\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-24 15:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579888800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 274.68,\n" +
            "\t\t\t\t\"feels_like\": 270.77,\n" +
            "\t\t\t\t\"temp_min\": 274.68,\n" +
            "\t\t\t\t\"temp_max\": 277.32,\n" +
            "\t\t\t\t\"pressure\": 1020,\n" +
            "\t\t\t\t\"sea_level\": 1020,\n" +
            "\t\t\t\t\"grnd_level\": 1012,\n" +
            "\t\t\t\t\"humidity\": 94,\n" +
            "\t\t\t\t\"temp_kf\": -2.64\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 45\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 2.89,\n" +
            "\t\t\t\t\"deg\": 97\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-24 18:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579899600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 275.69,\n" +
            "\t\t\t\t\"feels_like\": 271.42,\n" +
            "\t\t\t\t\"temp_min\": 275.69,\n" +
            "\t\t\t\t\"temp_max\": 277.45,\n" +
            "\t\t\t\t\"pressure\": 1021,\n" +
            "\t\t\t\t\"sea_level\": 1021,\n" +
            "\t\t\t\t\"grnd_level\": 1013,\n" +
            "\t\t\t\t\"humidity\": 95,\n" +
            "\t\t\t\t\"temp_kf\": -1.76\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 801,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"few clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"02n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 19\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.67,\n" +
            "\t\t\t\t\"deg\": 113\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-24 21:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579910400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.58,\n" +
            "\t\t\t\t\"feels_like\": 273.47,\n" +
            "\t\t\t\t\"temp_min\": 277.58,\n" +
            "\t\t\t\t\"temp_max\": 278.46,\n" +
            "\t\t\t\t\"pressure\": 1020,\n" +
            "\t\t\t\t\"sea_level\": 1020,\n" +
            "\t\t\t\t\"grnd_level\": 1012,\n" +
            "\t\t\t\t\"humidity\": 94,\n" +
            "\t\t\t\t\"temp_kf\": -0.88\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 39\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.87,\n" +
            "\t\t\t\t\"deg\": 120\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 00:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579921200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.83,\n" +
            "\t\t\t\t\"feels_like\": 275.07,\n" +
            "\t\t\t\t\"temp_min\": 278.83,\n" +
            "\t\t\t\t\"temp_max\": 278.83,\n" +
            "\t\t\t\t\"pressure\": 1019,\n" +
            "\t\t\t\t\"sea_level\": 1019,\n" +
            "\t\t\t\t\"grnd_level\": 1011,\n" +
            "\t\t\t\t\"humidity\": 95,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 86\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.75,\n" +
            "\t\t\t\t\"deg\": 134\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 03:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579932000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.52,\n" +
            "\t\t\t\t\"feels_like\": 273.61,\n" +
            "\t\t\t\t\"temp_min\": 277.52,\n" +
            "\t\t\t\t\"temp_max\": 277.52,\n" +
            "\t\t\t\t\"pressure\": 1018,\n" +
            "\t\t\t\t\"sea_level\": 1018,\n" +
            "\t\t\t\t\"grnd_level\": 1010,\n" +
            "\t\t\t\t\"humidity\": 96,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 68\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.65,\n" +
            "\t\t\t\t\"deg\": 138\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 06:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579942800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.12,\n" +
            "\t\t\t\t\"feels_like\": 274.52,\n" +
            "\t\t\t\t\"temp_min\": 278.12,\n" +
            "\t\t\t\t\"temp_max\": 278.12,\n" +
            "\t\t\t\t\"pressure\": 1019,\n" +
            "\t\t\t\t\"sea_level\": 1019,\n" +
            "\t\t\t\t\"grnd_level\": 1011,\n" +
            "\t\t\t\t\"humidity\": 95,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 800,\n" +
            "\t\t\t\t\t\"main\": \"Clear\",\n" +
            "\t\t\t\t\t\"description\": \"clear sky\",\n" +
            "\t\t\t\t\t\"icon\": \"01d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 2\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.33,\n" +
            "\t\t\t\t\"deg\": 134\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 09:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579953600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 283.02,\n" +
            "\t\t\t\t\"feels_like\": 279.57,\n" +
            "\t\t\t\t\"temp_min\": 283.02,\n" +
            "\t\t\t\t\"temp_max\": 283.02,\n" +
            "\t\t\t\t\"pressure\": 1018,\n" +
            "\t\t\t\t\"sea_level\": 1018,\n" +
            "\t\t\t\t\"grnd_level\": 1010,\n" +
            "\t\t\t\t\"humidity\": 83,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 800,\n" +
            "\t\t\t\t\t\"main\": \"Clear\",\n" +
            "\t\t\t\t\t\"description\": \"clear sky\",\n" +
            "\t\t\t\t\t\"icon\": \"01d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 1\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.97,\n" +
            "\t\t\t\t\"deg\": 159\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 12:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579964400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 284.12,\n" +
            "\t\t\t\t\"feels_like\": 281.38,\n" +
            "\t\t\t\t\"temp_min\": 284.12,\n" +
            "\t\t\t\t\"temp_max\": 284.12,\n" +
            "\t\t\t\t\"pressure\": 1016,\n" +
            "\t\t\t\t\"sea_level\": 1016,\n" +
            "\t\t\t\t\"grnd_level\": 1008,\n" +
            "\t\t\t\t\"humidity\": 82,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 41\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.26,\n" +
            "\t\t\t\t\"deg\": 153\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 15:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579975200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.96,\n" +
            "\t\t\t\t\"feels_like\": 276.71,\n" +
            "\t\t\t\t\"temp_min\": 279.96,\n" +
            "\t\t\t\t\"temp_max\": 279.96,\n" +
            "\t\t\t\t\"pressure\": 1016,\n" +
            "\t\t\t\t\"sea_level\": 1016,\n" +
            "\t\t\t\t\"grnd_level\": 1008,\n" +
            "\t\t\t\t\"humidity\": 95,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 49\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.35,\n" +
            "\t\t\t\t\"deg\": 134\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 18:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579986000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.54,\n" +
            "\t\t\t\t\"feels_like\": 276.13,\n" +
            "\t\t\t\t\"temp_min\": 279.54,\n" +
            "\t\t\t\t\"temp_max\": 279.54,\n" +
            "\t\t\t\t\"pressure\": 1016,\n" +
            "\t\t\t\t\"sea_level\": 1016,\n" +
            "\t\t\t\t\"grnd_level\": 1008,\n" +
            "\t\t\t\t\"humidity\": 95,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 94\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.46,\n" +
            "\t\t\t\t\"deg\": 159\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-25 21:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1579996800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.49,\n" +
            "\t\t\t\t\"feels_like\": 274.78,\n" +
            "\t\t\t\t\"temp_min\": 278.49,\n" +
            "\t\t\t\t\"temp_max\": 278.49,\n" +
            "\t\t\t\t\"pressure\": 1015,\n" +
            "\t\t\t\t\"sea_level\": 1015,\n" +
            "\t\t\t\t\"grnd_level\": 1007,\n" +
            "\t\t\t\t\"humidity\": 96,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 76\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.62,\n" +
            "\t\t\t\t\"deg\": 161\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 00:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580007600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.51,\n" +
            "\t\t\t\t\"feels_like\": 274.9,\n" +
            "\t\t\t\t\"temp_min\": 278.51,\n" +
            "\t\t\t\t\"temp_max\": 278.51,\n" +
            "\t\t\t\t\"pressure\": 1015,\n" +
            "\t\t\t\t\"sea_level\": 1015,\n" +
            "\t\t\t\t\"grnd_level\": 1007,\n" +
            "\t\t\t\t\"humidity\": 97,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 70\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.53,\n" +
            "\t\t\t\t\"deg\": 198\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 03:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580018400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 276.84,\n" +
            "\t\t\t\t\"feels_like\": 273.39,\n" +
            "\t\t\t\t\"temp_min\": 276.84,\n" +
            "\t\t\t\t\"temp_max\": 276.84,\n" +
            "\t\t\t\t\"pressure\": 1015,\n" +
            "\t\t\t\t\"sea_level\": 1015,\n" +
            "\t\t\t\t\"grnd_level\": 1007,\n" +
            "\t\t\t\t\"humidity\": 94,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 58\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 2.74,\n" +
            "\t\t\t\t\"deg\": 235\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 06:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580029200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.72,\n" +
            "\t\t\t\t\"feels_like\": 274.58,\n" +
            "\t\t\t\t\"temp_min\": 278.72,\n" +
            "\t\t\t\t\"temp_max\": 278.72,\n" +
            "\t\t\t\t\"pressure\": 1016,\n" +
            "\t\t\t\t\"sea_level\": 1016,\n" +
            "\t\t\t\t\"grnd_level\": 1008,\n" +
            "\t\t\t\t\"humidity\": 86,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 37\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 3.87,\n" +
            "\t\t\t\t\"deg\": 203\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 09:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580040000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 283.05,\n" +
            "\t\t\t\t\"feels_like\": 278.02,\n" +
            "\t\t\t\t\"temp_min\": 283.05,\n" +
            "\t\t\t\t\"temp_max\": 283.05,\n" +
            "\t\t\t\t\"pressure\": 1015,\n" +
            "\t\t\t\t\"sea_level\": 1015,\n" +
            "\t\t\t\t\"grnd_level\": 1007,\n" +
            "\t\t\t\t\"humidity\": 77,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 803,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"broken clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 56\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 5.89,\n" +
            "\t\t\t\t\"deg\": 200\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 12:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580050800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 283.47,\n" +
            "\t\t\t\t\"feels_like\": 278.51,\n" +
            "\t\t\t\t\"temp_min\": 283.47,\n" +
            "\t\t\t\t\"temp_max\": 283.47,\n" +
            "\t\t\t\t\"pressure\": 1014,\n" +
            "\t\t\t\t\"sea_level\": 1014,\n" +
            "\t\t\t\t\"grnd_level\": 1006,\n" +
            "\t\t\t\t\"humidity\": 82,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.22,\n" +
            "\t\t\t\t\"deg\": 204\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 15:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580061600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.6,\n" +
            "\t\t\t\t\"feels_like\": 276.76,\n" +
            "\t\t\t\t\"temp_min\": 281.6,\n" +
            "\t\t\t\t\"temp_max\": 281.6,\n" +
            "\t\t\t\t\"pressure\": 1013,\n" +
            "\t\t\t\t\"sea_level\": 1013,\n" +
            "\t\t\t\t\"grnd_level\": 1005,\n" +
            "\t\t\t\t\"humidity\": 93,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.04,\n" +
            "\t\t\t\t\"deg\": 200\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 18:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580072400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 282.5,\n" +
            "\t\t\t\t\"feels_like\": 276.31,\n" +
            "\t\t\t\t\"temp_min\": 282.5,\n" +
            "\t\t\t\t\"temp_max\": 282.5,\n" +
            "\t\t\t\t\"pressure\": 1011,\n" +
            "\t\t\t\t\"sea_level\": 1011,\n" +
            "\t\t\t\t\"grnd_level\": 1004,\n" +
            "\t\t\t\t\"humidity\": 87,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 7.94,\n" +
            "\t\t\t\t\"deg\": 200\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-26 21:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580083200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 282.72,\n" +
            "\t\t\t\t\"feels_like\": 274.92,\n" +
            "\t\t\t\t\"temp_min\": 282.72,\n" +
            "\t\t\t\t\"temp_max\": 282.72,\n" +
            "\t\t\t\t\"pressure\": 1010,\n" +
            "\t\t\t\t\"sea_level\": 1010,\n" +
            "\t\t\t\t\"grnd_level\": 1002,\n" +
            "\t\t\t\t\"humidity\": 89,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 10.43,\n" +
            "\t\t\t\t\"deg\": 205\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.69\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 00:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580094000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.97,\n" +
            "\t\t\t\t\"feels_like\": 277.54,\n" +
            "\t\t\t\t\"temp_min\": 281.97,\n" +
            "\t\t\t\t\"temp_max\": 281.97,\n" +
            "\t\t\t\t\"pressure\": 1010,\n" +
            "\t\t\t\t\"sea_level\": 1010,\n" +
            "\t\t\t\t\"grnd_level\": 1002,\n" +
            "\t\t\t\t\"humidity\": 88,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 5.31,\n" +
            "\t\t\t\t\"deg\": 250\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.5\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 03:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580104800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.13,\n" +
            "\t\t\t\t\"feels_like\": 277.04,\n" +
            "\t\t\t\t\"temp_min\": 281.13,\n" +
            "\t\t\t\t\"temp_max\": 281.13,\n" +
            "\t\t\t\t\"pressure\": 1010,\n" +
            "\t\t\t\t\"sea_level\": 1010,\n" +
            "\t\t\t\t\"grnd_level\": 1002,\n" +
            "\t\t\t\t\"humidity\": 90,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.67,\n" +
            "\t\t\t\t\"deg\": 217\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.19\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 06:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580115600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.76,\n" +
            "\t\t\t\t\"feels_like\": 277.28,\n" +
            "\t\t\t\t\"temp_min\": 281.76,\n" +
            "\t\t\t\t\"temp_max\": 281.76,\n" +
            "\t\t\t\t\"pressure\": 1009,\n" +
            "\t\t\t\t\"sea_level\": 1009,\n" +
            "\t\t\t\t\"grnd_level\": 1001,\n" +
            "\t\t\t\t\"humidity\": 91,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 5.48,\n" +
            "\t\t\t\t\"deg\": 209\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.5\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 09:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580126400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 282.62,\n" +
            "\t\t\t\t\"feels_like\": 277.3,\n" +
            "\t\t\t\t\"temp_min\": 282.62,\n" +
            "\t\t\t\t\"temp_max\": 282.62,\n" +
            "\t\t\t\t\"pressure\": 1007,\n" +
            "\t\t\t\t\"sea_level\": 1007,\n" +
            "\t\t\t\t\"grnd_level\": 999,\n" +
            "\t\t\t\t\"humidity\": 86,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.68,\n" +
            "\t\t\t\t\"deg\": 221\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.88\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 12:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580137200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.22,\n" +
            "\t\t\t\t\"feels_like\": 277.47,\n" +
            "\t\t\t\t\"temp_min\": 281.22,\n" +
            "\t\t\t\t\"temp_max\": 281.22,\n" +
            "\t\t\t\t\"pressure\": 1004,\n" +
            "\t\t\t\t\"sea_level\": 1004,\n" +
            "\t\t\t\t\"grnd_level\": 996,\n" +
            "\t\t\t\t\"humidity\": 93,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.36,\n" +
            "\t\t\t\t\"deg\": 229\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 2.63\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 15:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580148000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 280.87,\n" +
            "\t\t\t\t\"feels_like\": 277.11,\n" +
            "\t\t\t\t\"temp_min\": 280.87,\n" +
            "\t\t\t\t\"temp_max\": 280.87,\n" +
            "\t\t\t\t\"pressure\": 1001,\n" +
            "\t\t\t\t\"sea_level\": 1001,\n" +
            "\t\t\t\t\"grnd_level\": 993,\n" +
            "\t\t\t\t\"humidity\": 94,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.32,\n" +
            "\t\t\t\t\"deg\": 196\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.94\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 18:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580158800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.5,\n" +
            "\t\t\t\t\"feels_like\": 274.24,\n" +
            "\t\t\t\t\"temp_min\": 279.5,\n" +
            "\t\t\t\t\"temp_max\": 279.5,\n" +
            "\t\t\t\t\"pressure\": 999,\n" +
            "\t\t\t\t\"sea_level\": 999,\n" +
            "\t\t\t\t\"grnd_level\": 991,\n" +
            "\t\t\t\t\"humidity\": 92,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 501,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"moderate rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 5.95,\n" +
            "\t\t\t\t\"deg\": 232\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 9.63\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-27 21:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580169600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.62,\n" +
            "\t\t\t\t\"feels_like\": 273.46,\n" +
            "\t\t\t\t\"temp_min\": 279.62,\n" +
            "\t\t\t\t\"temp_max\": 279.62,\n" +
            "\t\t\t\t\"pressure\": 1000,\n" +
            "\t\t\t\t\"sea_level\": 1000,\n" +
            "\t\t\t\t\"grnd_level\": 991,\n" +
            "\t\t\t\t\"humidity\": 85,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.95,\n" +
            "\t\t\t\t\"deg\": 250\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.88\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 00:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580180400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 278.95,\n" +
            "\t\t\t\t\"feels_like\": 272.45,\n" +
            "\t\t\t\t\"temp_min\": 278.95,\n" +
            "\t\t\t\t\"temp_max\": 278.95,\n" +
            "\t\t\t\t\"pressure\": 1000,\n" +
            "\t\t\t\t\"sea_level\": 1000,\n" +
            "\t\t\t\t\"grnd_level\": 992,\n" +
            "\t\t\t\t\"humidity\": 85,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 7.26,\n" +
            "\t\t\t\t\"deg\": 247\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.69\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 03:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580191200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.2,\n" +
            "\t\t\t\t\"feels_like\": 270.61,\n" +
            "\t\t\t\t\"temp_min\": 277.2,\n" +
            "\t\t\t\t\"temp_max\": 277.2,\n" +
            "\t\t\t\t\"pressure\": 1002,\n" +
            "\t\t\t\t\"sea_level\": 1002,\n" +
            "\t\t\t\t\"grnd_level\": 994,\n" +
            "\t\t\t\t\"humidity\": 81,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.82,\n" +
            "\t\t\t\t\"deg\": 255\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.44\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 06:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580202000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.4,\n" +
            "\t\t\t\t\"feels_like\": 270.64,\n" +
            "\t\t\t\t\"temp_min\": 277.4,\n" +
            "\t\t\t\t\"temp_max\": 277.4,\n" +
            "\t\t\t\t\"pressure\": 1005,\n" +
            "\t\t\t\t\"sea_level\": 1005,\n" +
            "\t\t\t\t\"grnd_level\": 997,\n" +
            "\t\t\t\t\"humidity\": 76,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 37\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.9,\n" +
            "\t\t\t\t\"deg\": 252\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 09:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580212800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.11,\n" +
            "\t\t\t\t\"feels_like\": 272.47,\n" +
            "\t\t\t\t\"temp_min\": 281.11,\n" +
            "\t\t\t\t\"temp_max\": 281.11,\n" +
            "\t\t\t\t\"pressure\": 1006,\n" +
            "\t\t\t\t\"sea_level\": 1006,\n" +
            "\t\t\t\t\"grnd_level\": 998,\n" +
            "\t\t\t\t\"humidity\": 67,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 802,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"scattered clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"03d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 49\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 10.01,\n" +
            "\t\t\t\t\"deg\": 253\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 12:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580223600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 281.26,\n" +
            "\t\t\t\t\"feels_like\": 273.59,\n" +
            "\t\t\t\t\"temp_min\": 281.26,\n" +
            "\t\t\t\t\"temp_max\": 281.26,\n" +
            "\t\t\t\t\"pressure\": 1005,\n" +
            "\t\t\t\t\"sea_level\": 1005,\n" +
            "\t\t\t\t\"grnd_level\": 997,\n" +
            "\t\t\t\t\"humidity\": 70,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 99\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 8.81,\n" +
            "\t\t\t\t\"deg\": 249\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.38\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 15:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580234400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.61,\n" +
            "\t\t\t\t\"feels_like\": 272.55,\n" +
            "\t\t\t\t\"temp_min\": 279.61,\n" +
            "\t\t\t\t\"temp_max\": 279.61,\n" +
            "\t\t\t\t\"pressure\": 1006,\n" +
            "\t\t\t\t\"sea_level\": 1006,\n" +
            "\t\t\t\t\"grnd_level\": 998,\n" +
            "\t\t\t\t\"humidity\": 85,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 99\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 8.23,\n" +
            "\t\t\t\t\"deg\": 251\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.5\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 18:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580245200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.72,\n" +
            "\t\t\t\t\"feels_like\": 273.55,\n" +
            "\t\t\t\t\"temp_min\": 279.72,\n" +
            "\t\t\t\t\"temp_max\": 279.72,\n" +
            "\t\t\t\t\"pressure\": 1006,\n" +
            "\t\t\t\t\"sea_level\": 1006,\n" +
            "\t\t\t\t\"grnd_level\": 998,\n" +
            "\t\t\t\t\"humidity\": 85,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.99,\n" +
            "\t\t\t\t\"deg\": 246\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.44\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-28 21:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580256000,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.94,\n" +
            "\t\t\t\t\"feels_like\": 274.95,\n" +
            "\t\t\t\t\"temp_min\": 279.94,\n" +
            "\t\t\t\t\"temp_max\": 279.94,\n" +
            "\t\t\t\t\"pressure\": 1007,\n" +
            "\t\t\t\t\"sea_level\": 1007,\n" +
            "\t\t\t\t\"grnd_level\": 1000,\n" +
            "\t\t\t\t\"humidity\": 88,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 5.51,\n" +
            "\t\t\t\t\"deg\": 268\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.5\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-29 00:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580266800,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.99,\n" +
            "\t\t\t\t\"feels_like\": 273.25,\n" +
            "\t\t\t\t\"temp_min\": 277.99,\n" +
            "\t\t\t\t\"temp_max\": 277.99,\n" +
            "\t\t\t\t\"pressure\": 1009,\n" +
            "\t\t\t\t\"sea_level\": 1009,\n" +
            "\t\t\t\t\"grnd_level\": 1001,\n" +
            "\t\t\t\t\"humidity\": 90,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 99\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.72,\n" +
            "\t\t\t\t\"deg\": 248\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.13\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-29 03:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580277600,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 277.47,\n" +
            "\t\t\t\t\"feels_like\": 272.92,\n" +
            "\t\t\t\t\"temp_min\": 277.47,\n" +
            "\t\t\t\t\"temp_max\": 277.47,\n" +
            "\t\t\t\t\"pressure\": 1010,\n" +
            "\t\t\t\t\"sea_level\": 1010,\n" +
            "\t\t\t\t\"grnd_level\": 1002,\n" +
            "\t\t\t\t\"humidity\": 87,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04n\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 97\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.2,\n" +
            "\t\t\t\t\"deg\": 244\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"n\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-29 06:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580288400,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 279.53,\n" +
            "\t\t\t\t\"feels_like\": 274.88,\n" +
            "\t\t\t\t\"temp_min\": 279.53,\n" +
            "\t\t\t\t\"temp_max\": 279.53,\n" +
            "\t\t\t\t\"pressure\": 1011,\n" +
            "\t\t\t\t\"sea_level\": 1011,\n" +
            "\t\t\t\t\"grnd_level\": 1003,\n" +
            "\t\t\t\t\"humidity\": 88,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 500,\n" +
            "\t\t\t\t\t\"main\": \"Rain\",\n" +
            "\t\t\t\t\t\"description\": \"light rain\",\n" +
            "\t\t\t\t\t\"icon\": \"10d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 4.9,\n" +
            "\t\t\t\t\"deg\": 241\n" +
            "\t\t\t},\n" +
            "\t\t\t\"rain\": {\n" +
            "\t\t\t\t\"3h\": 0.13\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-29 09:00:00\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"dt\": 1580299200,\n" +
            "\t\t\t\"main\": {\n" +
            "\t\t\t\t\"temp\": 282.72,\n" +
            "\t\t\t\t\"feels_like\": 277.04,\n" +
            "\t\t\t\t\"temp_min\": 282.72,\n" +
            "\t\t\t\t\"temp_max\": 282.72,\n" +
            "\t\t\t\t\"pressure\": 1011,\n" +
            "\t\t\t\t\"sea_level\": 1011,\n" +
            "\t\t\t\t\"grnd_level\": 1004,\n" +
            "\t\t\t\t\"humidity\": 72,\n" +
            "\t\t\t\t\"temp_kf\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"weather\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"id\": 804,\n" +
            "\t\t\t\t\t\"main\": \"Clouds\",\n" +
            "\t\t\t\t\t\"description\": \"overcast clouds\",\n" +
            "\t\t\t\t\t\"icon\": \"04d\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"clouds\": {\n" +
            "\t\t\t\t\"all\": 100\n" +
            "\t\t\t},\n" +
            "\t\t\t\"wind\": {\n" +
            "\t\t\t\t\"speed\": 6.45,\n" +
            "\t\t\t\t\"deg\": 254\n" +
            "\t\t\t},\n" +
            "\t\t\t\"sys\": {\n" +
            "\t\t\t\t\"pod\": \"d\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"dt_txt\": \"2020-01-29 12:00:00\"\n" +
            "\t\t}\n" +
            "\t],\n" +
            "\t\"city\": {\n" +
            "\t\t\"id\": 2983989,\n" +
            "\t\t\"name\": \"Arrondissement de Rennes\",\n" +
            "\t\t\"coord\": {\n" +
            "\t\t\t\"lat\": 48.1667,\n" +
            "\t\t\t\"lon\": -1.6667\n" +
            "\t\t},\n" +
            "\t\t\"country\": \"FR\",\n" +
            "\t\t\"population\": 539984,\n" +
            "\t\t\"timezone\": 3600,\n" +
            "\t\t\"sunrise\": 1579851902,\n" +
            "\t\t\"sunset\": 1579884711\n" +
            "\t}\n" +
            "}";
    public static final String FIVEDAY_FORECAST_JSON = "{\n" +
            "\t\"Headline\": {\n" +
            "\t\t\"EffectiveDate\": \"2020-01-27T07:00:00+01:00\",\n" +
            "\t\t\"EffectiveEpochDate\": 1580104800,\n" +
            "\t\t\"Severity\": 2,\n" +
            "\t\t\"Text\": \"Expect rainy weather Monday morning through Monday evening\",\n" +
            "\t\t\"Category\": \"rain\",\n" +
            "\t\t\"EndDate\": \"2020-01-28T01:00:00+01:00\",\n" +
            "\t\t\"EndEpochDate\": 1580169600,\n" +
            "\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/extended-weather-forecast/132537?unit=c&lang=en-us\",\n" +
            "\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?unit=c&lang=en-us\"\n" +
            "\t},\n" +
            "\t\"DailyForecasts\": [\n" +
            "\t\t{\n" +
            "\t\t\t\"Date\": \"2020-01-24T07:00:00+01:00\",\n" +
            "\t\t\t\"EpochDate\": 1579845600,\n" +
            "\t\t\t\"Sun\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-24T08:45:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1579851900,\n" +
            "\t\t\t\t\"Set\": \"2020-01-24T17:53:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1579884780\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Moon\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-24T08:42:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1579851720,\n" +
            "\t\t\t\t\"Set\": \"2020-01-24T17:23:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1579882980,\n" +
            "\t\t\t\t\"Phase\": \"New\",\n" +
            "\t\t\t\t\"Age\": 0\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Temperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 3.6,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 6.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 2.8,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 6.7,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperatureShade\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 2.8,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 6.9,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"HoursOfSun\": 1.3,\n" +
            "\t\t\t\"DegreeDaySummary\": {\n" +
            "\t\t\t\t\"Heating\": {\n" +
            "\t\t\t\t\t\"Value\": 13.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Cooling\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"AirAndPollen\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"AirQuality\",\n" +
            "\t\t\t\t\t\"Value\": 29,\n" +
            "\t\t\t\t\t\"Category\": \"Good\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1,\n" +
            "\t\t\t\t\t\"Type\": \"Particle Pollution\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Grass\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Mold\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Ragweed\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Tree\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"UVIndex\",\n" +
            "\t\t\t\t\t\"Value\": 1,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"Day\": {\n" +
            "\t\t\t\t\"Icon\": 4,\n" +
            "\t\t\t\t\"IconPhrase\": \"Intermittent clouds\",\n" +
            "\t\t\t\t\"HasPrecipitation\": false,\n" +
            "\t\t\t\t\"ShortPhrase\": \"Sunny intervals\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Intervals of clouds and sunshine\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 25,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 1,\n" +
            "\t\t\t\t\"RainProbability\": 15,\n" +
            "\t\t\t\t\"SnowProbability\": 10,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 7.4,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 59,\n" +
            "\t\t\t\t\t\t\"Localized\": \"ENE\",\n" +
            "\t\t\t\t\t\t\"English\": \"ENE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 24.1,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 70,\n" +
            "\t\t\t\t\t\t\"Localized\": \"ENE\",\n" +
            "\t\t\t\t\t\t\"English\": \"ENE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 84\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Night\": {\n" +
            "\t\t\t\t\"Icon\": 36,\n" +
            "\t\t\t\t\"IconPhrase\": \"Intermittent clouds\",\n" +
            "\t\t\t\t\"HasPrecipitation\": false,\n" +
            "\t\t\t\t\"ShortPhrase\": \"Partly cloudy\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Partly cloudy\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 25,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 0,\n" +
            "\t\t\t\t\"RainProbability\": 25,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 7.4,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 107,\n" +
            "\t\t\t\t\t\t\"Localized\": \"ESE\",\n" +
            "\t\t\t\t\t\t\"English\": \"ESE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 11.1,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 121,\n" +
            "\t\t\t\t\t\t\"Localized\": \"ESE\",\n" +
            "\t\t\t\t\t\t\"English\": \"ESE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 46\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Sources\": [\n" +
            "\t\t\t\t\"AccuWeather\"\n" +
            "\t\t\t],\n" +
            "\t\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=1&unit=c&lang=en-us\",\n" +
            "\t\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=1&unit=c&lang=en-us\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"Date\": \"2020-01-25T07:00:00+01:00\",\n" +
            "\t\t\t\"EpochDate\": 1579932000,\n" +
            "\t\t\t\"Sun\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-25T08:44:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1579938240,\n" +
            "\t\t\t\t\"Set\": \"2020-01-25T17:55:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1579971300\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Moon\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-25T09:22:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1579940520,\n" +
            "\t\t\t\t\"Set\": \"2020-01-25T18:27:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1579973220,\n" +
            "\t\t\t\t\"Phase\": \"WaxingCrescent\",\n" +
            "\t\t\t\t\"Age\": 1\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Temperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 5.4,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 11.1,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 3.2,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 9.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperatureShade\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 3.2,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 9.6,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"HoursOfSun\": 4.6,\n" +
            "\t\t\t\"DegreeDaySummary\": {\n" +
            "\t\t\t\t\"Heating\": {\n" +
            "\t\t\t\t\t\"Value\": 10.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Cooling\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"AirAndPollen\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"AirQuality\",\n" +
            "\t\t\t\t\t\"Value\": 18,\n" +
            "\t\t\t\t\t\"Category\": \"Good\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1,\n" +
            "\t\t\t\t\t\"Type\": \"Particle Pollution\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Grass\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Mold\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Ragweed\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Tree\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"UVIndex\",\n" +
            "\t\t\t\t\t\"Value\": 2,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"Day\": {\n" +
            "\t\t\t\t\"Icon\": 4,\n" +
            "\t\t\t\t\"IconPhrase\": \"Intermittent clouds\",\n" +
            "\t\t\t\t\"HasPrecipitation\": false,\n" +
            "\t\t\t\t\"ShortPhrase\": \"Times of clouds and sun\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Intervals of clouds and sunshine\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 25,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 2,\n" +
            "\t\t\t\t\"RainProbability\": 25,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 11.1,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 140,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SE\",\n" +
            "\t\t\t\t\t\t\"English\": \"SE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 16.7,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 158,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSE\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 54\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Night\": {\n" +
            "\t\t\t\t\"Icon\": 12,\n" +
            "\t\t\t\t\"IconPhrase\": \"Showers\",\n" +
            "\t\t\t\t\"HasPrecipitation\": true,\n" +
            "\t\t\t\t\"PrecipitationType\": \"Rain\",\n" +
            "\t\t\t\t\"PrecipitationIntensity\": \"Light\",\n" +
            "\t\t\t\t\"ShortPhrase\": \"Cloudy with showers around\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Considerable cloudiness with a couple of showers\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 60,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 20,\n" +
            "\t\t\t\t\"RainProbability\": 60,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 9.3,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 177,\n" +
            "\t\t\t\t\t\t\"Localized\": \"S\",\n" +
            "\t\t\t\t\t\t\"English\": \"S\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 13.0,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 160,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSE\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSE\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 2.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 2.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 1.5,\n" +
            "\t\t\t\t\"HoursOfRain\": 1.5,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 96\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Sources\": [\n" +
            "\t\t\t\t\"AccuWeather\"\n" +
            "\t\t\t],\n" +
            "\t\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=2&unit=c&lang=en-us\",\n" +
            "\t\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=2&unit=c&lang=en-us\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"Date\": \"2020-01-26T07:00:00+01:00\",\n" +
            "\t\t\t\"EpochDate\": 1580018400,\n" +
            "\t\t\t\"Sun\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-26T08:43:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580024580,\n" +
            "\t\t\t\t\"Set\": \"2020-01-26T17:57:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580057820\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Moon\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-26T09:54:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580028840,\n" +
            "\t\t\t\t\"Set\": \"2020-01-26T19:33:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580063580,\n" +
            "\t\t\t\t\"Phase\": \"WaxingCrescent\",\n" +
            "\t\t\t\t\"Age\": 2\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Temperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 6.5,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 11.9,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 2.8,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 7.7,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperatureShade\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 2.8,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 8.2,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"HoursOfSun\": 1.4,\n" +
            "\t\t\t\"DegreeDaySummary\": {\n" +
            "\t\t\t\t\"Heating\": {\n" +
            "\t\t\t\t\t\"Value\": 9.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Cooling\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"AirAndPollen\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"AirQuality\",\n" +
            "\t\t\t\t\t\"Value\": 26,\n" +
            "\t\t\t\t\t\"Category\": \"Good\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1,\n" +
            "\t\t\t\t\t\"Type\": \"Particle Pollution\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Grass\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Mold\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Ragweed\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Tree\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"UVIndex\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"Day\": {\n" +
            "\t\t\t\t\"Icon\": 12,\n" +
            "\t\t\t\t\"IconPhrase\": \"Showers\",\n" +
            "\t\t\t\t\"HasPrecipitation\": true,\n" +
            "\t\t\t\t\"PrecipitationType\": \"Rain\",\n" +
            "\t\t\t\t\"PrecipitationIntensity\": \"Moderate\",\n" +
            "\t\t\t\t\"ShortPhrase\": \"A couple of afternoon showers\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Considerable cloudiness, becoming breezy in the afternoon with a couple of showers\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 68,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 20,\n" +
            "\t\t\t\t\"RainProbability\": 68,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 18.5,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 205,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 27.8,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 209,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 2.7,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 2.7,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 2.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 2.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 97\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Night\": {\n" +
            "\t\t\t\t\"Icon\": 36,\n" +
            "\t\t\t\t\"IconPhrase\": \"Intermittent clouds\",\n" +
            "\t\t\t\t\"HasPrecipitation\": true,\n" +
            "\t\t\t\t\"PrecipitationType\": \"Rain\",\n" +
            "\t\t\t\t\"PrecipitationIntensity\": \"Light\",\n" +
            "\t\t\t\t\"ShortPhrase\": \"A shower early; partly cloudy\",\n" +
            "\t\t\t\t\"LongPhrase\": \"A stray evening shower; otherwise, partly cloudy\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 43,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 20,\n" +
            "\t\t\t\t\"RainProbability\": 43,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 16.7,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 224,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 29.6,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 227,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.5,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.5,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.5,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.5,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 58\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Sources\": [\n" +
            "\t\t\t\t\"AccuWeather\"\n" +
            "\t\t\t],\n" +
            "\t\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=3&unit=c&lang=en-us\",\n" +
            "\t\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=3&unit=c&lang=en-us\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"Date\": \"2020-01-27T07:00:00+01:00\",\n" +
            "\t\t\t\"EpochDate\": 1580104800,\n" +
            "\t\t\t\"Sun\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-27T08:42:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580110920,\n" +
            "\t\t\t\t\"Set\": \"2020-01-27T17:58:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580144280\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Moon\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-27T10:20:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580116800,\n" +
            "\t\t\t\t\"Set\": \"2020-01-27T20:37:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580153820,\n" +
            "\t\t\t\t\"Phase\": \"WaxingCrescent\",\n" +
            "\t\t\t\t\"Age\": 3\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Temperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 5.5,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 10.4,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 0.6,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 3.6,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperatureShade\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 0.6,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 4.2,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"HoursOfSun\": 1.6,\n" +
            "\t\t\t\"DegreeDaySummary\": {\n" +
            "\t\t\t\t\"Heating\": {\n" +
            "\t\t\t\t\t\"Value\": 10.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Cooling\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"AirAndPollen\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"AirQuality\",\n" +
            "\t\t\t\t\t\"Value\": 30,\n" +
            "\t\t\t\t\t\"Category\": \"Good\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1,\n" +
            "\t\t\t\t\t\"Type\": \"Ozone\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Grass\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Mold\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Ragweed\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Tree\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"UVIndex\",\n" +
            "\t\t\t\t\t\"Value\": 1,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"Day\": {\n" +
            "\t\t\t\t\"Icon\": 18,\n" +
            "\t\t\t\t\"IconPhrase\": \"Rain\",\n" +
            "\t\t\t\t\"HasPrecipitation\": true,\n" +
            "\t\t\t\t\"PrecipitationType\": \"Rain\",\n" +
            "\t\t\t\t\"PrecipitationIntensity\": \"Light\",\n" +
            "\t\t\t\t\"ShortPhrase\": \"Windy with periods of rain\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Windy with periods of rain\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 65,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 0,\n" +
            "\t\t\t\t\"RainProbability\": 65,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 31.5,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 206,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 38.9,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 200,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 6.5,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 6.5,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 3.5,\n" +
            "\t\t\t\t\"HoursOfRain\": 3.5,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 96\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Night\": {\n" +
            "\t\t\t\t\"Icon\": 39,\n" +
            "\t\t\t\t\"IconPhrase\": \"Partly cloudy w/ showers\",\n" +
            "\t\t\t\t\"HasPrecipitation\": true,\n" +
            "\t\t\t\t\"PrecipitationType\": \"Rain\",\n" +
            "\t\t\t\t\"PrecipitationIntensity\": \"Moderate\",\n" +
            "\t\t\t\t\"ShortPhrase\": \"Early showers; clearing\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Showers in the evening; otherwise, clearing and breezy\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 95,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 20,\n" +
            "\t\t\t\t\"RainProbability\": 95,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 24.1,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 242,\n" +
            "\t\t\t\t\t\t\"Localized\": \"WSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"WSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 29.6,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 211,\n" +
            "\t\t\t\t\t\t\"Localized\": \"SSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"SSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 6.8,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 6.8,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 3.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 3.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 63\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Sources\": [\n" +
            "\t\t\t\t\"AccuWeather\"\n" +
            "\t\t\t],\n" +
            "\t\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=4&unit=c&lang=en-us\",\n" +
            "\t\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=4&unit=c&lang=en-us\"\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "\t\t\t\"Date\": \"2020-01-28T07:00:00+01:00\",\n" +
            "\t\t\t\"EpochDate\": 1580191200,\n" +
            "\t\t\t\"Sun\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-28T08:40:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580197200,\n" +
            "\t\t\t\t\"Set\": \"2020-01-28T18:00:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580230800\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Moon\": {\n" +
            "\t\t\t\t\"Rise\": \"2020-01-28T10:42:00+01:00\",\n" +
            "\t\t\t\t\"EpochRise\": 1580204520,\n" +
            "\t\t\t\t\"Set\": \"2020-01-28T21:42:00+01:00\",\n" +
            "\t\t\t\t\"EpochSet\": 1580244120,\n" +
            "\t\t\t\t\"Phase\": \"WaxingCrescent\",\n" +
            "\t\t\t\t\"Age\": 4\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Temperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 6.1,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 9.3,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperature\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 1.7,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 4.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"RealFeelTemperatureShade\": {\n" +
            "\t\t\t\t\"Minimum\": {\n" +
            "\t\t\t\t\t\"Value\": 1.7,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Maximum\": {\n" +
            "\t\t\t\t\t\"Value\": 3.8,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"HoursOfSun\": 2.6,\n" +
            "\t\t\t\"DegreeDaySummary\": {\n" +
            "\t\t\t\t\"Heating\": {\n" +
            "\t\t\t\t\t\"Value\": 10.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Cooling\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"C\",\n" +
            "\t\t\t\t\t\"UnitType\": 17\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"AirAndPollen\": [\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"AirQuality\",\n" +
            "\t\t\t\t\t\"Value\": 29,\n" +
            "\t\t\t\t\t\"Category\": \"Good\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1,\n" +
            "\t\t\t\t\t\"Type\": \"Ozone\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Grass\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Mold\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Ragweed\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"Tree\",\n" +
            "\t\t\t\t\t\"Value\": 0,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t{\n" +
            "\t\t\t\t\t\"Name\": \"UVIndex\",\n" +
            "\t\t\t\t\t\"Value\": 2,\n" +
            "\t\t\t\t\t\"Category\": \"Low\",\n" +
            "\t\t\t\t\t\"CategoryValue\": 1\n" +
            "\t\t\t\t}\n" +
            "\t\t\t],\n" +
            "\t\t\t\"Day\": {\n" +
            "\t\t\t\t\"Icon\": 6,\n" +
            "\t\t\t\t\"IconPhrase\": \"Mostly cloudy\",\n" +
            "\t\t\t\t\"HasPrecipitation\": false,\n" +
            "\t\t\t\t\"ShortPhrase\": \"Increasingly windy\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Increasingly windy with variable cloudiness\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 25,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 0,\n" +
            "\t\t\t\t\"RainProbability\": 25,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 27.8,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 252,\n" +
            "\t\t\t\t\t\t\"Localized\": \"WSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"WSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 40.7,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 248,\n" +
            "\t\t\t\t\t\t\"Localized\": \"WSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"WSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 75\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Night\": {\n" +
            "\t\t\t\t\"Icon\": 38,\n" +
            "\t\t\t\t\"IconPhrase\": \"Mostly cloudy\",\n" +
            "\t\t\t\t\"HasPrecipitation\": false,\n" +
            "\t\t\t\t\"ShortPhrase\": \"Clouds breaking\",\n" +
            "\t\t\t\t\"LongPhrase\": \"Clouds breaking\",\n" +
            "\t\t\t\t\"PrecipitationProbability\": 25,\n" +
            "\t\t\t\t\"ThunderstormProbability\": 0,\n" +
            "\t\t\t\t\"RainProbability\": 25,\n" +
            "\t\t\t\t\"SnowProbability\": 0,\n" +
            "\t\t\t\t\"IceProbability\": 0,\n" +
            "\t\t\t\t\"Wind\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 20.4,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 246,\n" +
            "\t\t\t\t\t\t\"Localized\": \"WSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"WSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"WindGust\": {\n" +
            "\t\t\t\t\t\"Speed\": {\n" +
            "\t\t\t\t\t\t\"Value\": 40.7,\n" +
            "\t\t\t\t\t\t\"Unit\": \"km/h\",\n" +
            "\t\t\t\t\t\t\"UnitType\": 7\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"Direction\": {\n" +
            "\t\t\t\t\t\t\"Degrees\": 244,\n" +
            "\t\t\t\t\t\t\"Localized\": \"WSW\",\n" +
            "\t\t\t\t\t\t\"English\": \"WSW\"\n" +
            "\t\t\t\t\t}\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"TotalLiquid\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Rain\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Snow\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"cm\",\n" +
            "\t\t\t\t\t\"UnitType\": 4\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"Ice\": {\n" +
            "\t\t\t\t\t\"Value\": 0.0,\n" +
            "\t\t\t\t\t\"Unit\": \"mm\",\n" +
            "\t\t\t\t\t\"UnitType\": 3\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"HoursOfPrecipitation\": 0.0,\n" +
            "\t\t\t\t\"HoursOfRain\": 0.0,\n" +
            "\t\t\t\t\"HoursOfSnow\": 0.0,\n" +
            "\t\t\t\t\"HoursOfIce\": 0.0,\n" +
            "\t\t\t\t\"CloudCover\": 96\n" +
            "\t\t\t},\n" +
            "\t\t\t\"Sources\": [\n" +
            "\t\t\t\t\"AccuWeather\"\n" +
            "\t\t\t],\n" +
            "\t\t\t\"MobileLink\": \"http://m.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=5&unit=c&lang=en-us\",\n" +
            "\t\t\t\"Link\": \"http://www.accuweather.com/en/fr/rennes/132537/daily-weather-forecast/132537?day=5&unit=c&lang=en-us\"\n" +
            "\t\t}\n" +
            "\t]\n" +
            "}";
    public static final String CONS = "[{\"Version\":1,\"Key\":\"132537\",\"Type\":\"City\",\"Rank\":41,\"LocalizedName\":\"Rennes\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"35\",\"LocalizedName\":\"Ille-et-Vilaine\"}},{\"Version\":1,\"Key\":\"177376\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennertshofen\",\"Country\":{\"ID\":\"DE\",\"LocalizedName\":\"Allemagne\"},\"AdministrativeArea\":{\"ID\":\"BY\",\"LocalizedName\":\"Bavière\"}},{\"Version\":1,\"Key\":\"180372\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennerod\",\"Country\":{\"ID\":\"DE\",\"LocalizedName\":\"Allemagne\"},\"AdministrativeArea\":{\"ID\":\"RP\",\"LocalizedName\":\"Rhénanie-Palatinat\"}},{\"Version\":1,\"Key\":\"160813\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Renneval\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"02\",\"LocalizedName\":\"Aisne\"}},{\"Version\":1,\"Key\":\"159330\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennes-le-Château\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"11\",\"LocalizedName\":\"Aude\"}},{\"Version\":1,\"Key\":\"159331\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennes-les-Bains\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"11\",\"LocalizedName\":\"Aude\"}},{\"Version\":1,\"Key\":\"158874\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennes-sur-Loue\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"25\",\"LocalizedName\":\"Doubs\"}},{\"Version\":1,\"Key\":\"160022\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Renneville\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"31\",\"LocalizedName\":\"Haute-Garonne\"}},{\"Version\":1,\"Key\":\"158571\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennepont\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"52\",\"LocalizedName\":\"Haute-Marne\"}},{\"Version\":1,\"Key\":\"159182\",\"Type\":\"City\",\"Rank\":83,\"LocalizedName\":\"Rennemoulin\",\"Country\":{\"ID\":\"FR\",\"LocalizedName\":\"France\"},\"AdministrativeArea\":{\"ID\":\"78\",\"LocalizedName\":\"Yvelines\"}}]";
}
