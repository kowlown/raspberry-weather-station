
package com.mystnihon.station.rest.accuweather.dto;

import java.io.Serializable;
import java.time.OffsetDateTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sun implements Serializable
{

    @SerializedName("Rise")
    @Expose
    private OffsetDateTime rise;
    @SerializedName("EpochRise")
    @Expose
    private int epochRise;
    @SerializedName("Set")
    @Expose
    private String set;
    @SerializedName("EpochSet")
    @Expose
    private int epochSet;
    private final static long serialVersionUID = -6269726824247004860L;

    public OffsetDateTime getRise() {
        return rise;
    }

    public void setRise(OffsetDateTime rise) {
        this.rise = rise;
    }

    public int getEpochRise() {
        return epochRise;
    }

    public void setEpochRise(int epochRise) {
        this.epochRise = epochRise;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public int getEpochSet() {
        return epochSet;
    }

    public void setEpochSet(int epochSet) {
        this.epochSet = epochSet;
    }

}
