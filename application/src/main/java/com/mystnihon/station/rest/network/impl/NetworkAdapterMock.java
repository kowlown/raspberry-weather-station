package com.mystnihon.station.rest.network.impl;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mystnihon.station.rest.accuweather.api.AccuWeatherApi;
import com.mystnihon.station.rest.accuweather.dto.FiveDayForecastData;
import com.mystnihon.station.rest.accuweather.dto.autocomplete.AutoCompleteResponse;
import com.mystnihon.station.rest.gson.adapters.OffsetDateTimeTypeAdapter;
import com.mystnihon.station.rest.network.NetworkAdapter;
import com.mystnihon.station.rest.owm.api.OpenWeatherMapApi;
import com.mystnihon.station.rest.owm.dto.ForecastData;
import com.mystnihon.station.rest.owm.dto.WeatherData;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.mystnihon.station.rest.network.impl.mock.Constants.CONS;
import static com.mystnihon.station.rest.network.impl.mock.Constants.FIVEDAY_FORECAST_JSON;
import static com.mystnihon.station.rest.network.impl.mock.Constants.FORECAST_JSON;
import static com.mystnihon.station.rest.network.impl.mock.Constants.WEATHER_JSON;


/**
 * Default template
 * Created by Lévi LIRVAT on 25/11/2014.
 */
@Slf4j
@Profile("mock")
@Component
public class NetworkAdapterMock implements NetworkAdapter {
    private static final OffsetDateTimeTypeAdapter offsetDateTimeTypeAdapter = new OffsetDateTimeTypeAdapter();

    private final OpenWeatherMapApi openWeatherMapApi;
    private final AccuWeatherApi accuWeatherApi;
    private final Gson gson;

    public NetworkAdapterMock() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        gsonBuilder.registerTypeAdapter(OffsetDateTime.class, offsetDateTimeTypeAdapter);

        this.gson = gsonBuilder.create();
        openWeatherMapApi = new OpenWeatherMapApi() {
            @Override
            public Observable<ForecastData> getForecast(String cityCode, String appId) {
                return Observable.just(gson.fromJson(FORECAST_JSON, ForecastData.class));
            }

            @Override
            public Observable<WeatherData> getWeather(String cityCode, String appId) {
                return Observable.just(gson.fromJson(WEATHER_JSON, WeatherData.class));
            }
        };
        accuWeatherApi = new AccuWeatherApi() {
            @Override
            public Observable<FiveDayForecastData> get5DayForecast(String locationKey, String apiKeyAccuW) {
                log.info("MOCK get5DayForecast for param {}, {}", locationKey, apiKeyAccuW);
                return Observable.just(gson.fromJson(FIVEDAY_FORECAST_JSON, FiveDayForecastData.class));
            }

            @Override
            public Observable<List<AutoCompleteResponse>> autoCompleteLocation(String locationSearch, String apiKeyAccuW) {
                Type listType = new TypeToken<ArrayList<AutoCompleteResponse>>() {
                }.getType();
                return Observable.just(gson.fromJson(CONS, listType));
            }
        };
    }

    public OpenWeatherMapApi getOpenWeatherMapApi() {
        return openWeatherMapApi;
    }

    public AccuWeatherApi getAccuWeatherApi() {
        return accuWeatherApi;
    }


}
