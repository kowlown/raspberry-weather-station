
package com.mystnihon.station.rest.accuweather.dto.autocomplete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class AutoCompleteResponse {

    @SerializedName("Version")
    @Expose
    private long version;
    @SerializedName("Key")
    @Expose
    private String key;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Rank")
    @Expose
    private long rank;
    @SerializedName("LocalizedName")
    @Expose
    private String localizedName;
    @SerializedName("Country")
    @Expose
    private Country country;
    @SerializedName("AdministrativeArea")
    @Expose
    private AdministrativeArea administrativeArea;


}
