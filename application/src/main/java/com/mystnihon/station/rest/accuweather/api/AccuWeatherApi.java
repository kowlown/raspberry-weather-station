package com.mystnihon.station.rest.accuweather.api;

import com.mystnihon.station.rest.accuweather.dto.FiveDayForecastData;
import com.mystnihon.station.rest.accuweather.dto.autocomplete.AutoCompleteResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface AccuWeatherApi {
    String OPEN_WEATHER_MAP_FORECAST_PATH = "/forecasts/v1/daily/5day/{locationKey}?details=true&metric=true";

    @GET(OPEN_WEATHER_MAP_FORECAST_PATH)
    Observable<FiveDayForecastData> get5DayForecast(@Path("locationKey") String locationKey, @Query("apikey") String apiKeyAccuW);

    @GET("/locations/v1/cities/autocomplete?language=fr")
    Observable<List<AutoCompleteResponse>> autoCompleteLocation(@Query("q") String locationSearch, @Query("apikey") String apiKeyAccuW);
}
