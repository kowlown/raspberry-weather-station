package com.mystnihon.station.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {

    private OpenWeatherMap openWeatherMap;
    private AccuWeather accuWeather;

    public static class OpenWeatherMap {
        private String baseUrl;
        private String appId;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }
    }

    public static class AccuWeather {
        private String baseUrl;
        private String apiKey;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }
    }


    public OpenWeatherMap getOpenWeatherMap() {
        return openWeatherMap;
    }

    public void setOpenWeatherMap(OpenWeatherMap openWeatherMap) {
        this.openWeatherMap = openWeatherMap;
    }

    public AccuWeather getAccuWeather() {
        return accuWeather;
    }

    public void setAccuWeather(AccuWeather accuWeather) {
        this.accuWeather = accuWeather;
    }
}
