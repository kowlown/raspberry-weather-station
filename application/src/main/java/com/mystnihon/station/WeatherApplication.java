package com.mystnihon.station;

import com.mystnihon.station.util.IconUtil;
import com.mystnihon.station.util.ResourceConstantName;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Locale;
import java.util.ResourceBundle;

public class WeatherApplication extends Application {

    private Parent root;
    private ConfigurableApplicationContext springContext;

    @Override
    public void init() throws Exception {

        springContext = new SpringApplicationBuilder(StartApplication.class)
                .headless(false).run();
        springContext.getAutowireCapableBeanFactory().autowireBean(this);
        ResourceBundle bundle = ResourceBundle.getBundle(ResourceConstantName.RESOURCE_BUNDLE, Locale.getDefault());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/scenes/main.fxml"), bundle);
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
    }


    @Override
    public void start(Stage primaryStage) {
//        primaryStage.initStyle(StageStyle.UNDECORATED);
//        primaryStage.setFullScreen(true);
        IconUtil.setIcon(primaryStage);
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnShown(event -> {

        });
    }

    @Override
    public void stop() {
        springContext.close();
        Platform.exit();
    }
}
