#include "receiver.h"

using namespace std;

RF24 radio(RPI_BPLUS_GPIO_J8_26, RPI_BPLUS_GPIO_J8_24, BCM2835_SPI_SPEED_8MHZ);
const uint8_t pipes[][7] = {"master","slave","idle"};


//SPI spi;

//Initialization
void setup() {
    radio.begin();
    radio.setAutoAck(1);
    radio.setRetries(15, 15);
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1, pipes[1]);
    radio.powerUp();
    radio.printDetails(); // Dump the configuration of the rf unit for debugging
    radio.startListening();
}

//We want to listen and wait from a message if have one or return an empty string.
//It's synchronous.
Data listen() {
    Data data = {};
    cout << "listen" << endl;
    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    bool timeout = false;

    // While nothing is received
    while (!radio.available() && !timeout) {
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        auto duration = chrono::duration_cast<chrono::seconds>(end - start);
        delay(5); //Sleep for 5 ms
        // If waited longer than 1 seconds, indicate timeout and exit while loop
        if (duration.count() > 1) {
            timeout = true;
        }
    }

    if (timeout) {
        cout << "Failed, response timed out." << endl;
        data.type = -1;
    } else {
        cout << "Read" << endl;
        radio.read(&data, sizeof(Data));
        cout << "\nRadio is ON | " << endl;
        //radio.writeAckPayload(counter,counter , sizeof(counter));
        printf(" Value = %f ", data.value);
    }
    delay(1);
    return data;
}

void terminate() {
//    radio.stopListening();
//    radio.closeReadingPipe(1);
//    spi.end();
}
