package com.mystnihon.transceiver;

import java.util.Objects;


/**
 * This class manage the asynchronous of the the message received with event listener.
 */
class Transceiver implements Runnable {
    private static boolean loaded;

    static {
        try {
            System.loadLibrary("receiver");
            loaded = true;
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Error\n" + e.toString());
            loaded = false;
        }
    }

    private final MessageListener messageListener;
    private volatile boolean run = false;

    Transceiver(MessageListener messageListener) {
        Objects.requireNonNull(messageListener);
        this.messageListener = messageListener;
    }

    @Override
    public void run() {
        if (!loaded) return;
        run = true;
        initialize();
        //We want to periodically check if we have a message and send a callback if so.
        while (run) {
            read();
            sleep(10);
            read();
            sleep(10);
            read();
            sleep(3010);
        }
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            release();
            System.err.println("Er:" + e.getMessage());
        }
    }

    private void read() {
        Data message = readMessage();
        if (message != null) {
            messageReceived(message);
        } else {
            System.err.println("message null");
        }

    }

    //region Native methods

    private void initialize() {
        Receiver.setup();
    }

    private Data readMessage() {
        try {
            return Receiver.listen();
        } catch (IllegalArgumentException e) {
            System.err.println("Error:\n" + e.getMessage());
            return null;
        }
    }

    private void nativeRelease() {
        Receiver.terminate();
    }

    //endregion

    void release() {
        if (!loaded) return;
        run = false;
        nativeRelease();
    }

    private void messageReceived(Data message) {
        //messageListener is always not null. Test is not needed.
        messageListener.onMessageReceived(message);
    }
}
