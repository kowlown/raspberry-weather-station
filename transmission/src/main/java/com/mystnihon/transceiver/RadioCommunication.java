package com.mystnihon.transceiver;

import java.util.Optional;

public class RadioCommunication implements MessageListener {

    private static final String RADIO = "radio";
    private final Transceiver transceiver;
    private MessageListener messageListener;

    public RadioCommunication() {
        transceiver = new Transceiver(this);
    }

    @Override
    public void onMessageReceived(Data message) {
        Optional.ofNullable(messageListener).ifPresent(s -> s.onMessageReceived(message));
    }

    public void start() {
        Thread thread = new Thread(transceiver, RADIO);
        thread.start();
    }

    public void stop() {
        transceiver.release();
    }

    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }
}
