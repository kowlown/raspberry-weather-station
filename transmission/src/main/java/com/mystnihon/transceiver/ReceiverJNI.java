/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.mystnihon.transceiver;

public class ReceiverJNI {
  public final static native void Data_type_set(long jarg1, Data jarg1_, int jarg2);
  public final static native int Data_type_get(long jarg1, Data jarg1_);
  public final static native void Data_value_set(long jarg1, Data jarg1_, float jarg2);
  public final static native float Data_value_get(long jarg1, Data jarg1_);
  public final static native long new_Data();
  public final static native void delete_Data(long jarg1);
  public final static native void setup();
  public final static native long listen();
  public final static native void terminate();
}
